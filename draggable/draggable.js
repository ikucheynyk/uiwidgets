/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 *
 * @fileoverview Draggable module, enables dragging posibility for html elements, adds events to easily
 * control dragging
 *
 * CONFIG OPTIONS
 * container - html element that moves over the screen while dragging
 * element - html element that is possible to point to start dragging the container
 * displayDragging - (clone|original) determines dragging|resizing visualization. default `clone`
 * allowDrag - enables dragging. default true
 * allowResize - enables resizing. default false
 * allowResizeHeight - enables resizing vertically. default true
 * allowResizeWidth - enables resizing horizontally. default true
 * minWidth - sets minimal width for the container
 * maxWidth - sets maximal width for the container
 * minHeight - sets minimal height for the container
 * maxHeight - sets maximal height for the container
 * displayMoveCursor - enables displaying `move` mouse pointer when hovering the element. default true
 *
 * EVENTS
 * mouseDown - fired when mouse left button has beed pressed over the element
 * mouseMove - fired when mouse pressed and mooved over element
 * resize - fired when resizing the container
 * drag - fired when dragging the container
 * dragEnd - fired after dragging has finished and mouse button released
 * resizeEnd - fired after resizing has finished and mouse button released
 * mouseUp - fired after resizing or dragging when mouse button released
 *
 * EXAMPLE OF USAGE
 * 	<script type="text/javascript">
 *	new UITools.Draggable({
 *		container: document.getElementById('elem1'),
 *		maxHeight: 300,
 *		maxWidth: 300,
 *		minHeight: 100,
 *		minWidth: 100,
 *		eventListeners: {
 *			mouseUp: function(oMouseEvent, oOffset) {
 *				var el = document.getElementById('elem1');
 *				el.style.top = oOffset.top + 'px';
 *				el.style.left = oOffset.left + 'px';
 *				el.style.width = oOffset.width + 'px';
 *				el.style.height = oOffset.height + 'px';
 *			}
 *		}
 *	});
 *	</script>
 *
 */

if (typeof UITools == 'undefined') {
	alert('uitools.js is required to run UITools.Draggable');
}

/**
 * Constructor of the object
 * @constructor
 * @param oArgs {object} descr
 */
UITools.Draggable = function(oArgs) {
	// Call superclass constructor
	UITools.Draggable.superconstructor.call(this, oArgs);
};

// inherit basic functionality
UITools.inherit(UITools.Draggable, UITools.Widget);

/**
 * Initialize
 */
UITools.Draggable.prototype.init = function(oArgs) {
    UITools.Draggable.superclass.init.call(this, oArgs);

    // container is required
    this.container = this.element(oArgs.container);
    if(!this.isElement(this.container)) {
        alert('UITools.Menu init failed: container missing or invalid');
    }

	this.draggableElement = this.element(oArgs.draggableElement) || this.container;
	this.displayDragging = this.config.displayDragging || 'clone';
	this.eventListeners = this.config.eventListeners || {};
	this.allowDrag = this.config.allowDrag!=null ? this.config.allowDrag : true;
	this.allowResize = this.config.allowResize!=null ? this.config.allowResize : false;
	this.allowResizeHeight = this.config.allowResizeHeight!=null ? this.config.allowResizeHeight : true;
	this.allowResizeWidth = this.config.allowResizeWidth!=null ? this.config.allowResizeWidth : true;
	this.minWidth = this.config.minWidth;
	this.maxWidth = this.config.maxWidth;
	this.maxHeight = this.config.maxHeight;
	this.minHeight = this.config.minHeight;
	this.displayMoveCursor = this.config.displayMoveCursor!=null ? this.config.displayMoveCursor : true;

	this.drag = false;
	this.resize = false;
	this.elementHovered = false;
	this.containerHovered = false;
	this.resizeDirection = null;
	this.containerOffset = {};
	this.startDragMousePosition = {
		top: 0,
		left: 0
	};


	this.drag = false;
	var self = this;
	UITools.addEvent(document, 'mousemove', function(oEvent) {
		self.mouseMove(oEvent);
	}, false);
	UITools.addEvent(this.draggableElement, 'mousedown', function(oEvent) {
		self.mouseDown(oEvent, true);
		UITools.stopEvent(oEvent);
	}, false);
	UITools.addEvent(this.container, 'mousedown', function(oEvent) {
		self.mouseDown(oEvent);
		UITools.stopEvent(oEvent);
	}, false);
	UITools.addEvent(document, 'mouseup', function(oEvent) {
		self.mouseUp(oEvent);
	}, false);
	UITools.addEvent(this.container, 'mouseover', function(oEvent) {
		self.containerHovered = true;
	}, false);
	UITools.addEvent(this.container, 'mouseout', function(oEvent) {
		self.containerHovered = false;
	}, false);
	UITools.addEvent(this.draggableElement, 'mouseover', function(oEvent) {
		self.elementHovered = true;
	}, false);
	UITools.addEvent(this.draggableElement, 'mouseout', function(oEvent) {
		self.elementHovered = false;
	}, false);
};

UITools.Draggable.prototype.mouseDown = function(oEvent, draggingTriggerClicked) {
	if(!this.allowResize && !this.allowDrag && draggingTriggerClicked) {
		return;
	}
	// if resize or drag has started, avoid duplicates
	if(this.resize || this.drag) {
		return;
	}
	if(this.allowResize) {
		this.resizeDirection = this.getResizeDirection(oEvent);
		this.containerOffset = UITools.getElementOffset(this.container);
		if(this.resizeDirection) {
			this.resize = true;
			this.containerHovered = false;
		}
	}
	if(draggingTriggerClicked) {
		// Drag start
		this.drag = true;
	}
	var offset = UITools.getElementOffset(this.container);
	this.startDragMousePosition = {
		top: oEvent.clientY - offset.top,
		left: oEvent.clientX - offset.left
	};
	this.startCursorPosition = {
		top: oEvent.clientY,
		left: oEvent.clientX 
	};

	if(this.displayDragging=='clone') {
		this.cloned = this.container.cloneNode(true);
		this.cloned.style.position = 'absolute';
		this.cloned.style.left = (oEvent.clientX - this.startDragMousePosition.left) + 'px';
		this.cloned.style.top = (oEvent.clientY - this.startDragMousePosition.top) + 'px';
		this.cloned.style.opacity = 0.2;
		if(this.cloned.filters && this.cloned.filters.alpha) {
			this.cloned.filters.alpha.opacity = 20;
		}
		// Insert cloned element after original
//		if(this.container.parentNode.lastchild == this.container) {
//			this.container.parentNode.appendChild(this.cloned);
//		} else {
//			this.container.parentNode.insertBefore(this.cloned, this.container.nextSibling);
//		}
		document.body.appendChild(this.cloned);
	} else if(this.displayDragging=='original') {
		this.cloned = this.container;
		this.cloned.style.position = 'absolute';
		this.cloned.style.left = (oEvent.clientX - this.startDragMousePosition.left) + 'px';
		this.cloned.style.top = (oEvent.clientY - this.startDragMousePosition.top) + 'px';
	}
	if(this.eventListeners.mouseDown) {
		this.eventListeners.mouseDown(oEvent, UITools.getElementOffset(this.cloned));
	}
}

UITools.Draggable.prototype.mouseMove = function(oEvent) {
	if(!this.allowDrag && !this.allowResize) {
		return;
	}
	var sResizeDirection;
	// Display coresponding cursor
	if(this.allowResize && this.resize) {
		var iWidth, iLeft, iHeight, iTop;
		if(this.allowResizeHeight) {
			if(/n/.test(this.resizeDirection)) {
				iHeight = (this.startCursorPosition.top - oEvent.clientY + this.containerOffset.height);
				if(!(this.minHeight && this.minHeight>iHeight) && !(this.maxHeight && this.maxHeight<iHeight)) {
					this.cloned.style.top = (this.containerOffset.top - (this.startCursorPosition.top - oEvent.clientY)) + 'px';
					this.cloned.style.height = iHeight + 'px';
				}
			}
			if(/s/.test(this.resizeDirection)) {
				iHeight = (oEvent.clientY - this.startCursorPosition.top + this.containerOffset.height);
				if(!(this.minHeight && this.minHeight>iHeight) && !(this.maxHeight && this.maxHeight<iHeight)) {
					this.cloned.style.height = iHeight + 'px';
				}
			}
		}
		if(this.allowResizeWidth) {
			if(/e/.test(this.resizeDirection)) {
				iWidth = (oEvent.clientX - this.startCursorPosition.left + this.containerOffset.width);
				if(!(this.minWidth && this.minWidth>iWidth) && !(this.maxWidth && this.maxWidth<iWidth)) {
					this.cloned.style.width = iWidth + 'px';
				}
			}
			if(/w/.test(this.resizeDirection)) {
				iWidth = (this.startCursorPosition.left - oEvent.clientX + this.containerOffset.width);
				if(!(this.minWidth && this.minWidth>iWidth) && !(this.maxWidth && this.maxWidth<iWidth)) {
					this.cloned.style.left = (this.containerOffset.left - (this.startCursorPosition.left - oEvent.clientX)) + 'px';
					this.cloned.style.width = iWidth + 'px';
				}
			}
		}
		if(this.eventListeners.mouseMove) {
			this.eventListeners.mouseMove(oEvent, UITools.getElementOffset(this.cloned), this.container);
		}
		if(this.eventListeners.resize) {
			this.eventListeners.resize(oEvent, UITools.getElementOffset(this.cloned), this.container);
		}
		return;
	}
	// Set cursor
	if(this.containerHovered) {
	    if(this.allowResize) {
			sResizeDirection = this.getResizeDirection(oEvent);
			if(sResizeDirection) {
				this.container.style.cursor = sResizeDirection + '-resize';
				return;
			}
	    }
		if(this.allowDrag && this.elementHovered && this.displayMoveCursor) {
			this.container.style.cursor = 'move';
			return;
		}
		this.container.style.cursor = 'auto';
	}
	// Drag
	if(this.allowDrag && this.drag) {
		this.cloned.style.left = (oEvent.clientX - this.startDragMousePosition.left) + 'px';
		this.cloned.style.top = (oEvent.clientY - this.startDragMousePosition.top) + 'px';
		if(this.eventListeners.mouseMove) {
			this.eventListeners.mouseMove(oEvent, UITools.getElementOffset(this.cloned), this.container);
		}
		if(this.eventListeners.drag) {
			this.eventListeners.drag(oEvent, UITools.getElementOffset(this.cloned), this.container);
		}
	}
}

UITools.Draggable.prototype.mouseUp = function(oEvent) {
	if(!this.allowDrag && !this.allowResize) {
		return;
	}
	var oCloneOffset = UITools.getElementOffset(this.cloned);
	if(this.displayDragging=='clone') {
		if(this.cloned && this.cloned.parentNode) {
			this.cloned.parentNode.removeChild(this.cloned);
		}
	}
	this.cloned = null;
	if(this.drag && this.allowDrag) {
		this.drag = false;
		if(this.eventListeners.dragEnd) {
			this.eventListeners.dragEnd(oEvent, oCloneOffset, this.container);
		}
		if(this.eventListeners.mouseUp) {
			this.eventListeners.mouseUp(oEvent, oCloneOffset, this.container);
		}
	}
	if(this.resize && this.allowResize) {
		this.resize = false;
		if(this.eventListeners.resizeEnd) {
			this.eventListeners.resizeEnd(oEvent, oCloneOffset, this.container);
		}
		if(this.eventListeners.mouseUp) {
			this.eventListeners.mouseUp(oEvent, oCloneOffset, this.container);
		}
	}
}

UITools.Draggable.prototype.getResizeDirection = function(oEvent) {
	var vertical = '';
	var horizontal = '';
	var oOffset =  UITools.getElementOffset(this.container);
	if(oEvent.clientY > oOffset.top && oEvent.clientY < oOffset.top + 15 && this.allowResizeHeight)  {
		vertical = 'n';
	}
	if(oEvent.clientY < (oOffset.top + oOffset.height) && oEvent.clientY > (oOffset.top + oOffset.height - 15) && this.allowResizeHeight)  {
		vertical = 's';
	}
	if(oEvent.clientX > oOffset.left && oEvent.clientX < oOffset.left + 15 && this.allowResizeWidth)  {
		horizontal = 'w';
	}
	if(oEvent.clientX < (oOffset.left + oOffset.width) && oEvent.clientX > (oOffset.left + oOffset.width - 15) && this.allowResizeWidth)  {
		horizontal = 'e';
	}
	return vertical + horizontal;
}