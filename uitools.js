// initialize namespace object
if(typeof UITools == 'undefined') {
	UITools = function() {};
}

// define objects array
UITools.objects = [];

// inherit
UITools.inherit = function(Child, Parent) {
	var F = function() { }
	F.prototype = Parent.prototype
	Child.prototype = new F()
	Child.prototype.constructor = Child
    Child.superclass = Parent.prototype
    Child.superconstructor = Parent;
};


/**
 * This is a blank class, which is used as superclass for all UITools widgets
 * It holds all of interface functions needed
 */ 

UITools.Widget = function(oArgs) {
    // Keep config
    this.config = oArgs;
    // Define theme
    this.theme = this.config.theme ? this.config.theme : 'default';
	// Register instance in objects array
	this.objectId = UITools.objects.length;
	UITools.objects.push(this);
	// Keeps events here
	this.events = {};
    // register event listeners
    this.addEventListeners();
    // Initialize
	this.init(oArgs);
};

UITools.Widget.prototype.init = function(oArgs) {};

/**
 * Converts element id to reference.
 *
 * @param {string} element Element id
 * @return Reference to element
 * @type object
 */
UITools.Widget.prototype.element = function(element) {
	if (typeof element == 'string') {
		return document.getElementById(element);
	}
	return element;
};

/**
 * Checks if passed object is DOM element
 *
 * @param {object} DOM object
 * @return True or false
 * @type object
 */
UITools.Widget.prototype.isElement = function(obj) {
  try {
    //Using W3 DOM2 (works for FF, Opera and Chrom)
    return obj instanceof HTMLElement;
  }
  catch(e){
    //Browsers not supporting W3 DOM2 don't have HTMLElement and
    //an exception is thrown and we end up here. Testing some
    //properties that all elements have. (works on IE7)
    return (typeof obj==="object") &&
      (obj.nodeType===1) && (typeof obj.style === "object") &&
      (typeof obj.ownerDocument ==="object");
  }
};

// define eventlisteners array
UITools.Widget.prototype.events = {};

/**
 * Fires an event
 */
UITools.Widget.prototype.fireEvent = function(sEvent) {
    var oEvents = this.events;
    if (!oEvents[sEvent]) {
        return;
    }
    // Duplicate array because it may be modified from within the listeners
    var aListeners = oEvents[sEvent].listeners.slice();
    var iListeners = aListeners.length;
    var aArgs, oListener;
    for (var iListener = 0; iListener < iListeners; iListener++) {
        // Remove first argument
        aArgs = [].slice.call(arguments, 1);
        // Call event listener in scope of this object
        oListener = aListeners[iListener];
        if (typeof oListener == 'function') {
            oListener.apply(this, aArgs);
        } else {
            oListener.listener.apply(this, aArgs);
        }
    }
    // Remove one-time event listeners
    this.removeOnetimeEventListeners(sEvent);
};

/**
 * Creates multiple event listeners
 * @param oEventListeners {object} List of event listeners
 */
UITools.Widget.prototype.addEventListeners = function() {
	var oListeners = this.config.eventListeners;
	var fListener, iListeners, iListener;
	for (var sEvent in oListeners) {
		if (oListeners.hasOwnProperty(sEvent)) {
			vListener = oListeners[sEvent];
			if (vListener instanceof Array) {
				iListeners = vListener.length;
				for (iListener = 0; iListener < iListeners; iListener++) {
					this.addEventListener(sEvent, vListener[iListener]);
				}
			} else {
				this.addEventListener(sEvent, vListener);
			}
		}
	}
};


/**
 * Creates event listener
 * @param eventName {string} The name of event
 * @param eventFunction {function} Callback function
 */
UITools.Widget.prototype.addEventListener = function(sEvent, fListener, bOnetime) {
    if (typeof fListener != 'function') {
        return false;
    }
    var oEvents = this.events;
    var oEvent = oEvents[sEvent];
    if (!oEvent) {
        oEvents[sEvent] = {
            listeners: []
        };
        oEvent = oEvents[sEvent];
    } else {
        this.removeEventListener(sEvent, fListener);
    }
    if (bOnetime) {
        oEvent.listeners.push({
            listener: fListener,
            onetime: true
        });
    } else {
        oEvent.listeners.push(fListener);
    }
};

UITools.Widget.prototype.removeEventListener = function(sEvent, fListener) {
	var oEvents = this.events;
	if (!oEvents[sEvent]) {
		return 0;
	}
	var aListeners = oEvents[sEvent].listeners;
	var iRemoved = 0;
	var oListener;
	for (var iListener = aListeners.length - 1; iListener >= 0; iListener--) {
		oListener = aListeners[iListener];
		if (oListener == fListener || oListener.listener == fListener) {
			aListeners.splice(iListener, 1);
			iRemoved++;
		}
	}
	return iRemoved;
};

UITools.Widget.prototype.removeOnetimeEventListeners = function(sEvent) {
	var oEvents = this.events;
	if (!oEvents[sEvent]) {
		return 0;
	}
	var aListeners = oEvents[sEvent].listeners;
	var iRemoved = 0;
	for (var iListener = aListeners.length - 1; iListener >= 0; iListener--) {
		if (aListeners[iListener].onetime) {
			aListeners.splice(iListener, 1);
			iRemoved++;
		}
	}
	return iRemoved;
};





UITools.isArray = function(obj) {
	return obj.constructor == Array;
};


/**
 * Returns absolute position of the element on the page and its dimensions.
 *
 * @private
 * @param {object} oEl Element object
 * @return Offset: left or x - left offset; top or y - top offset,
 * width - element width, height - element height
 * @object
 */
UITools.getElementOffset = function(oEl) {
	// Check arguments
	if (!oEl) {
		return;
	}
	var iLeft = iTop = iWidth = iHeight = 0;
	var sTag;
	if (typeof oEl.getBoundingClientRect == 'function') {
		// IE
		var oRect = oEl.getBoundingClientRect();
		iLeft = oRect.left;
		iTop = oRect.top;
		iWidth = oRect.right - iLeft;
		iHeight = oRect.bottom - iTop;
		// getBoundingClientRect returns coordinates relative to the window
		iLeft += UITools.getPageScrollX();
		iTop += UITools.getPageScrollY();
		if (UITools.is_ie) {
			// Why "- 2" is explained here:
			// http://msdn.microsoft.com/library/default.asp?url=/workshop/author/dhtml/reference/methods/getboundingclientrect.asp
			iLeft -= 2;
			iTop -= 2;
		}
	} else {
		// Others
		iWidth = oEl.offsetWidth;
		iHeight = oEl.offsetHeight;
		var sPos = UITools.getStyleProperty(oEl, 'position');
		if (sPos == 'fixed') {
			iLeft = oEl.offsetLeft + UITools.getPageScrollX();
			iTop = oEl.offsetTop + UITools.getPageScrollY();
		} else if (sPos == 'absolute') {
			while (oEl) {
				sTag = oEl.tagName;
				if (sTag) {
					sTag = sTag.toLowerCase();
					// Safari adds margin of the body to offsetLeft and offsetTop values
					if (sTag != 'body' && sTag != 'html' || UITools.is_khtml) {
						iLeft += parseInt(oEl.offsetLeft, 10) || 0;
						iTop += parseInt(oEl.offsetTop, 10) || 0;
					}
				}
				oEl = oEl.offsetParent;
				sTag = oEl ? oEl.tagName : null;
				if (sTag) {
					sTag = sTag.toLowerCase();
					if (sTag != 'body' && sTag != 'html') {
						iLeft -= oEl.scrollLeft;
						iTop -= oEl.scrollTop;
					}
				}
			}
		} else {
			var bMoz = (UITools.is_gecko && !UITools.is_khtml);
			var fStyle = UITools.getStyleProperty;
			var oP = oEl;
			while (oP) {
				// -moz-box-sizing must be "border-box" to prevent subtraction of body
				// border in Mozilla
				if (bMoz) {
					sTag = oP.tagName;
					if (sTag) {
						sTag = sTag.toLowerCase();
						if (sTag == 'body' && !(fStyle(oP, '-moz-box-sizing') == 'border-box')) {
							iLeft += parseInt(fStyle(oP, 'border-left-width'));
							iTop += parseInt(fStyle(oP, 'border-top-width'));
						}
					}
				}
				iLeft += parseInt(oP.offsetLeft, 10) || 0;
				iTop += parseInt(oP.offsetTop, 10) || 0;
				oP = oP.offsetParent;
			}
			oP = oEl;
			while (oP.parentNode) {
				oP = oP.parentNode;
				sTag = oP.tagName;
				if (sTag) {
					sTag = sTag.toLowerCase();
					if (sTag != 'body' && sTag != 'html' && sTag != 'tr') {
						iLeft -= oP.scrollLeft;
						iTop -= oP.scrollTop;
					}
				}
			}
		}
	}
	return {
		left: iLeft,
		top: iTop,
		x: iLeft,
		y: iTop,
		width: iWidth,
		height: iHeight
	};
};

/**
 * Returns current document vertical scroll position.
 *
 * @return Current document vertical scroll position
 * @type number
 */
UITools.getPageScrollY = function() {
	if (window.pageYOffset) {
		return window.pageYOffset;
	} else if (document.body && document.body.scrollTop) {
		return document.body.scrollTop;
	} else if (document.documentElement && document.documentElement.scrollTop) {
		return document.documentElement.scrollTop;
	}
	return 0;
};

/**
 * Returns current document horizontal scroll position.
 *
 * @return Current document horizontal scroll position
 * @type number
 */
UITools.getPageScrollX = function() {
	if (window.pageXOffset) {
		return window.pageXOffset;
	} else if (document.body && document.body.scrollLeft) {
		return document.body.scrollLeft;
	} else if (document.documentElement && document.documentElement.scrollLeft) {
		return document.documentElement.scrollLeft;
	}
	return 0;
};

/**
 * Retrieves computed CSS property values from an element.
 *
 * @param {object} oEl Element object
 * @param {string} sPr Property name of element's style, e.g. "borderLeftWidth"
 * @return Computed CSS property value
 * @type string
 */
UITools.getStyleProperty = function(oEl, sPr) {
	var oDV = document.defaultView;
	if (oDV && oDV.getComputedStyle) {
		// Standard
		var oCS = oDV.getComputedStyle(oEl, '');
		if (oCS) {
			sPr = sPr.replace(/([A-Z])/g, '-$1').toLowerCase();
			return oCS.getPropertyValue(sPr);
		}
	} else if (oEl.currentStyle) {
		// IE
		return oEl.currentStyle[sPr];
	}
	// None
	return oEl.style[sPr];
};

/// detect Opera browser
UITools.is_opera = /opera/i.test(navigator.userAgent);

/// detect a special case of "web browser"
UITools.is_ie = ( /msie/i.test(navigator.userAgent) && !UITools.is_opera );

/// detect IE6.0/Win
UITools.is_ie6 = ( UITools.is_ie && /msie 6\.0/i.test(navigator.userAgent) );

/// detect IE7.0/Win
UITools.is_ie7 = ( UITools.is_ie && /msie 7\.0/i.test(navigator.userAgent) );

/// detect IE8.0/Win
UITools.is_ie8 = ( UITools.is_ie && /msie 8\.0/i.test(navigator.userAgent) );

/// detect IE for Macintosh
UITools.is_mac_ie = ( /msie.*mac/i.test(navigator.userAgent) && !UITools.is_opera );

/// detect KHTML-based browsers
UITools.is_khtml = /Chrome|Safari|Konqueror|AppleWebKit|KHTML/i.test(navigator.userAgent);

/// detect Konqueror
UITools.is_konqueror = /Konqueror/i.test(navigator.userAgent);

/// detect Gecko
UITools.is_gecko = /Gecko/i.test(navigator.userAgent);

/// detect WebKit
UITools.is_webkit = /WebKit/i.test(navigator.userAgent);

/// detect WebKit version
UITools.webkitVersion = UITools.is_webkit?parseInt(navigator.userAgent.replace(
				/.+WebKit\/([0-9]+)\..+/, "$1")):-1;
/// detect Google Chrome
UITools.is_gchrome = /Chrome/i.test(navigator.userAgent);

/**
 * Returns current browser's window size without scrollbars. Calls
 * {@link UITools#getWindowSize}, subtracts browser's scroll bar size, and
 * returns the result.
 *
 * <pre>
 * Returned object format:
 * {
 *   width: [number] window width in pixels,
 *   height: [number] window height in pixels
 * }
 * </pre>
 *
 * @return Window size without scrollbars
 * @type object
 */
UITools.getWindowDimensions = function() {
	// Get window size
	var oSize = UITools.getWindowSize();
	// Exception may occur if this function is called from <head>
	try {
		// Subtract scrollbars
		var iScrollX = window.pageXOffset || document.body.scrollLeft ||
		 document.documentElement.scrollLeft || 0;
		var iScrollY = window.pageYOffset || document.body.scrollTop ||
		 document.documentElement.scrollTop || 0;
		return {
			width: oSize.width - iScrollX,
			height: oSize.height - iScrollY
		};
	} catch (oException) {
		return oSize;
	};
};

/**
 * Returns current browser's window size. This is the usable size and does not
 * include the browser's menu and buttons.
 *
 * <pre>
 * Returned object format:
 * {
 *   width: [number] window width in pixels,
 *   height: [number] window height in pixels
 * }
 * </pre>
 *
 * @return Window size
 * @type object
 */
UITools.getWindowSize = function() {
	var iWidth = 0;
	var iHeight = 0;
	// Exception may occur if this function is called from <head>
	try {
		if (UITools.is_khtml) {
			iWidth = window.innerWidth || 0;
			iHeight = window.innerHeight || 0;
		} else if (document.compatMode && document.compatMode == 'CSS1Compat') {
			// Standards-compliant mode
			iWidth = document.documentElement.clientWidth || 0;
			iHeight = document.documentElement.clientHeight || 0;
		} else {
			// Non standards-compliant mode
			iWidth = document.body.clientWidth || 0;
			iHeight = document.body.clientHeight || 0;
		}
	} catch (oException) {};
	return {
		width: iWidth,
		height: iHeight
	};
};

/**
 * Makes a copy of an object.
 *
 * @param {object} oSrc Source object to clone
 */
UITools.clone = function(oSrc) {
	// If object and not null
	if (typeof oSrc == 'object' && oSrc) {
		var oClone = new oSrc.constructor();
		for (var sProp in oSrc) {
			oClone[sProp] = UITools.clone(oSrc[sProp]);
		}
		return oClone;
	}
	return oSrc;
};

/**
 * Stops bubbling and propagation of an event.
 *
 * <pre>
 * Note for Safari:
 * If it doesn't work and listener is called, you can check returnValue property
 * of the event in the listener. If it is false, don't proceed further because
 * event has been stopped.
 * </pre>
 *
 * @param {object} oEvent Optional event object. Default: window.event.
 * @return Always false
 * @type boolean
 */
UITools.stopEvent = function(oEvent) {
	oEvent || (oEvent = window.event);
	if (oEvent) {
		if (oEvent.stopPropagation) {
			oEvent.stopPropagation();
		}
		oEvent.cancelBubble = true;
		if (oEvent.preventDefault) {
			oEvent.preventDefault();
		}
		oEvent.returnValue = false;
	}
	return false;
};


UITools.removeOnUnload = [];

/**
 * Adds event handler to certain element or widget using DOM addEventListener or
 * MSIE attachEvent method. Doing this means that you can add multiple handlers
 * for the same object and event, and they will be called in order.
 *
 * @param {object} oElement Element object
 * @param {string} sEvent Event name
 * @param {function} fListener Event listener
 * @param {boolean} bUseCapture Optional. Default: false. For details see
 *	http://www.w3.org/TR/2000/REC-DOM-Level-2-Events-20001113/events.html#Events-EventTarget-addEventListener
 * @param {boolean} bRemoveOnUnload Optional. Default: true. remove eventlistener
 *	on page unload.
 */
UITools.addEvent = function(oElement, sEvent, fListener, bUseCapture, bRemoveOnUnload) {
	if (oElement.addEventListener) {
		// W3C
		if (!bUseCapture) {
			bUseCapture = false;
		}
		oElement.addEventListener(sEvent, fListener, bUseCapture);
	} else if (oElement.attachEvent) {
		// IE
		oElement.detachEvent('on' + sEvent, fListener);
		oElement.attachEvent('on' + sEvent, fListener);
		if (bUseCapture) {
			oElement.setCapture(false);
		}
	}
	if (typeof bRemoveOnUnload == 'undefined') {
		bRemoveOnUnload = true;
	}
	if (bRemoveOnUnload && UITools.is_ie6) {
		UITools.removeOnUnload.push({
			'element': oElement,
			'event': sEvent,
			'listener': fListener,
			'capture': bUseCapture
		});
	}
};


/**
 * Removes event handler added with {@link UITools.Utils#addEvent}.
 *
 * @param {object} oElement Element object
 * @param {string} sEvent Event name
 * @param {function} fListener Event listener
 * @param {boolean} bUseCapture Optional. Default: false. For details see
 * http://www.w3.org/TR/2000/REC-DOM-Level-2-Events-20001113/events.html#Events-EventTarget-removeEventListener
 */
UITools.removeEvent = function(oElement, sEvent, fListener, bUseCapture) {
	if (oElement.removeEventListener) {
		// W3C
		if (!bUseCapture) {
			bUseCapture = false;
		}
		oElement.removeEventListener(sEvent, fListener, bUseCapture);
	} else if (oElement.detachEvent) {
		// IE
		oElement.detachEvent('on' + sEvent, fListener);
	}
	for (var iLis = UITools.removeOnUnload.length - 1; iLis >= 0; iLis--) {
		var oParams = UITools.removeOnUnload[iLis];
		if (!oParams) {
			continue;
		}
		if (oElement == oParams['element'] && sEvent == oParams['event'] &&
		 fListener == oParams['listener'] && bUseCapture == oParams['capture']) {
			UITools.removeOnUnload[iLis] = null;
			UITools.removeEvent(
			 oParams['element'],
			 oParams['event'],
			 oParams['listener'],
			 oParams['capture']
			);
		}
	}
};


UITools.getElementsByClassName = function(className, tag, elm){
	var tag = tag || "*";
	var elm = elm || document;
	var elements = (tag == "*" && elm.all)? elm.all : elm.getElementsByTagName(tag);
	var returnElements = [];
	var current, aClasses;
	var length = elements.length;
	for(var i=0; i<length; i++) {
		current = elements[i];
		aClasses = current.className.split(' ');
		if(UITools.inArray(aClasses, className)) {
			returnElements.push(current);
		}
	}
	return returnElements;
};

UITools.removeClass = function(el, className) {
	if (!el || !el.className) {
		return;
	}
	var aClassNames = el.className.split(" ");
	for (var i = aClassNames.length; i > 0;) {
		if (aClassNames[--i] == className) {
			aClassNames.splice(i, 1);
		}
	}
	el.className = aClassNames.join(" ");
};

UITools.addClass = function(el, className) {
	if (!el || el.className==null) {
		return;
	}
	UITools.removeClass(el, className);
	el.className += " " + className;
};

UITools.inArray = function(array, needle) {
	var i;
	var len = array.length;
	for(i=0; i<len; i++) {
		if(array[i]==needle) {
			return true;
		}
	}
	return false;
};

UITools.findAndRemove = function(arr, needle) {
	if (!arr || !(arr instanceof Array)) {
		return;
	}
	for (var i = arr.length; i > 0;) {
		if (arr[--i] == needle) {
			arr.splice(i, 1);
		}
	}
	return arr;
};


/**
 * Create DOM element from text. Useful when you need to create node from HTML 
 * fragment and add it to the DOM tree.
 * @param {Object} txt HTML string
 * @return Created object
 * @type Object
 */
UITools.convertHTML2DOM = function(txt){
	// return null if no content given
	if(!txt){
		return null;
	}

	// create temp container
	var el = document.createElement("div");
	el.innerHTML = txt;

	var currEl = el.firstChild;
	
	// search for first element node
	while(currEl != null && (!currEl.nodeType || currEl.nodeType != 1)){
		currEl = currEl.nextSibling;
	}
	
	UITools.destroy(currEl);

	return currEl;
};

/**
 * Destroys the given element (remove it from the DOM tree) if it's not null
 * and it's parent is not null.
 *
 * @param el [HTMLElement] the element to destroy.
 */
UITools.destroy = function(el) {
	if (el && el.parentNode) {
		el.parentNode.removeChild(el);
	}
};


UITools.insertAfter = function(oNewNode, oExistingNode) {
	if(!oNewNode || !oExistingNode) {
		return;
	}
	// Insert cloned element after original
	if(oExistingNode.parentNode.lastchild == oExistingNode) {
		oExistingNode.parentNode.appendChild(oNewNode);
	} else {
		oExistingNode.parentNode.insertBefore(oNewNode, oExistingNode.nextSibling);
	}
};




UITools.jsonEncode = function(v, bAllowFunctions) {
	var a = [];
	var e = function(s) {
		a[a.length] = s;
	};
	var g = function(x) {
		var c, i, l, v;
		switch (typeof x) {
		case 'object':
			if (x) {
				if (x instanceof Array) {
					e('[');
					l = a.length;
					for (i = 0; i < x.length; i += 1) {
						if (l < a.length) {
							e(',');
						}
						g(x[i]);
					}
					e(']');
					return;
				} else if (x instanceof Date) {
					e('"');
					e(x.toString());
					e('"');
					return;
				} else if (typeof x.toString != 'undefined') {
					e('{');
					l = a.length;
					for (i in x) {
						v = x[i];
						if (x.hasOwnProperty(i) && typeof v != 'undefined' &&
							(bAllowFunctions || typeof v != 'function')) {
							if (l < a.length) {
								e(',');
							}
							g(i);
							e(':');
							g(v);
						}
					}
					e('}');
					return;
				}
			}
			e('null');
			return;
		case 'number':
			e(isFinite(x) ? +x : 'null');
			return;
		case 'string':
			l = x.length;
			e('"');
			for (i = 0; i < l; i += 1) {
				c = x.charAt(i);
				if (c >= ' ') {
					if (c == '\\' || c == '"') {
						e('\\');
					}
					e(c);
				} else {
					switch (c) {
						case '\b':
							e('\\b');
							break;
						case '\f':
							e('\\f');
							break;
						case '\n':
							e('\\n');
							break;
						case '\r':
							e('\\r');
							break;
						case '\t':
							e('\\t');
							break;
						default:
							c = c.charCodeAt();
							e('\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16));
					}
				}
			}
			e('"');
			return;
		case 'boolean':
			e(String(x));
			return;
		case 'function':
			if (bAllowFunctions) {
				e(x.toString().replace(/function anonymous/g, 'function'));
			} else {
				e('null');
			}
			return;
		default:
			e('null');
			return;
		}
	};
	g(v);
	return a.join('');
};



UITools.jsonDecode = function(text) {
	var p = /^\s*(([,:{}\[\]])|"(\\.|[^\x00-\x1f"\\])*"|-?\d+(\.\d*)?([eE][+-]?\d+)?|true|false|null)\s*/,
			token,
			operator;
	function error(m, t) {
			throw {
					name: 'JSONError',
					message: m,
					text: t || operator || token
			};
	}
	function next(b) {
			if (b && b != operator) {
					error("Expected '" + b + "'");
			}
			if (text) {
					var t = p.exec(text);
					if (t) {
							if (t[2]) {
									token = null;
									operator = t[2];
							} else {
									operator = null;
									try {
											token = eval(t[1]);
									} catch (e) {
											error("Bad token", t[1]);
									}
							}
							text = text.substring(t[0].length);
					} else {
							error("Unrecognized token", text);
					}
			} else {
					// undefined changed to null because it is not supported in IE 5.0
					token = operator = null;
			}
	}
	function val() {
			var k, o;
			switch (operator) {
			case '{':
					next('{');
					o = {};
					if (operator != '}') {
							for (;;) {
									if (operator || typeof token != 'string') {
											error("Missing key");
									}
									k = token;
									next();
									next(':');
									o[k] = val();
									if (operator != ',') {
											break;
									}
									next(',');
							}
					}
					next('}');
					return o;
			case '[':
					next('[');
					o = [];
					if (operator != ']') {
							for (;;) {
									o.push(val());
									if (operator != ',') {
											break;
									}
									next(',');
							}
					}
					next(']');
					return o;
			default:
					if (operator !== null) {
							error("Missing value");
					}
					k = token;
					next();
					return k;
			}
	}
	next();
	return val();
};




// Date constructor
UITools.Date = function() {};

UITools.Date.weekdayNames = [
	'sunday',
	'monday',
	'tuesday',
	'wednesday',
	'thursday',
	'friday',
	'saturday',
];
UITools.Date.shortWeekdayNames = [
	'sun',
	'mon',
	'tue',
	'wed',
	'thu',
	'fri',
	'sat',
];
UITools.Date.monthNames = [
	'january',
	'february',
	'march',
	'april',
	'may',
	'june',
	'july',
	'august',
	'september',
	'october',
	'november',
	'december'
];
UITools.Date.shortMonthNames = [
	'jan',
	'feb',
	'mar',
	'apr',
	'may',
	'jun',
	'jul',
	'aug',
	'sep',
	'oct',
	'nov',
	'dec'
];


UITools.Date.encode = function(dDate, sFormat) {
    if(!(dDate instanceof Date)) {
        return null;
    }
    var rule = {
        // Locale's abbreviated weekday name.
        '%a': function(date) {
            return UITools.Date.shortWeekdayNames[date.getDay()];
        },
        // Locale's full weekday name.
        '%A': function(date) {
            return UITools.Date.weekdayNames[date.getDay()];
        },
        // Locale's abbreviated month name.
        '%b': function(date) {
            return UITools.Date.shortMonthNames[date.getMonth()];
        },
        // Locale's full month name.
        '%B': function(date) {
            return UITools.Date.monthNames[date.getMonth()];
        },
        // Locale's appropriate date and time representation.
        '%c': function(date) {
        },
        // Century (a year divided by 100 and truncated to an integer) as a decimal number [00,99].
        '%C': function(date) {
            return Math.floor(date.getFullYear() / 100) + 1;
        },
        // Day of the month as a decimal number [01,31].
        '%d': function(date) {
            var d = date.getDate();
            return (d < 10) ? ("0" + d) : d;
        },
        // Date in the format mm/dd/yy.
        '%D': function(date) {
            return UITools.Date.encode(date, '%m/%d/%y');
        },
        // Day of the month as a decimal number [1,31] in a two-digit field with leading space character fill.
        '%e': function(date) {
            return date.getDate();
        },
        // Date in the format mm/dd/yy.
        '%h': function(date) {
            return rule['%b'](date);
        },
        // Hour (24-hour clock) as a decimal number [00,23].
        '%H': function(date) {
            var h = date.getHours();
            return (h < 10) ? ("0" + h) : h;
        },
        // Hour (12-hour clock) as a decimal number [01,12].
        '%I': function(date) {
            var h = date.getHours();
            var h12 = (h > 12) ? (h - 12) : h;
            return (h12 < 10) ? ("0" + h12) : h12;
        },
        // Day of the year as a decimal number [001,366].
        '%j': function(date) {
            var period = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0)
                - new Date(date.getFullYear(), 0, 0, 0, 0, 0);
            var d = Math.round(period / 86400000);
            return (d < 100) ? ((d < 10) ? ("00" + d) : ("0" + d)) : d;
        },
        // hour, range 0 to 23 (24h format)
        '%k': function(date) {
            return date.getHours();
        },
        // hour, range 1 to 12 (12h format)
        '%l': function(date) {
            var h = date.getHours();
            return (h > 12) ? (h - 12) : h;
        },
        // Month as a decimal number [01,12].
        '%m': function(date) {
            var m = date.getMonth();
            return (m < 9) ? ("0" + (1+m)) : (1+m);
        },
        // Minute as a decimal number [00,59].
        '%M': function(date) {
            var min = date.getMinutes();
            return (min < 10) ? ("0" + min) : min;
        },
        // A <newline> character
        '%n': function(date) {
            return "\n";
        },
        // am or pm
        '%p': function(date) {
            return (date.getHours() >= 12) ? 'pm' : 'am';
        },
        // AM or PM.
        '%P': function(date) {
            return (date.getHours() >= 12) ? 'PM' : 'AM';
        },
        // 12-hour clock time [01,12] using the AM/PM notation; in the POSIX locale, this shall be equivalent to %I : %M : %S %p.
        '%r': function(date) {
            return UITools.Date.encode(date, '%I : %M : %S %p');
        },
        // Seconds as a decimal number [00,60].
        '%S': function(date) {
            var sec = date.getSeconds();
            return (sec < 10) ? ("0" + sec) : sec;
        },
        // A <tab> character
        '%t': function(date) {
            return "\t";
        },
        // 24-hour clock time [00,23] in the format HH:MM:SS.
        '%T': function(date) {
            return UITools.Date.encode(date, '%H:%M:%S');
        },
        // Weekday as a decimal number [1,7] (1=Monday).
        '%u': function(date) {
            var w = date.getDay();
            return (w == 0) ? 7 : w;
        },
        // Week of the year (Sunday as the first day of the week) as a decimal number [00,53]. All days in a new year preceding the first Sunday shall be considered to be in week 0.
        '%U': function(date) {
            var d = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
            var DoW = d.getDay();
            d.setDate(d.getDate() - (DoW + 6) % 7 + 3); // Nearest Thu
            var ms = d.valueOf(); // GMT
            d.setMonth(0);
            d.setDate(4); // Thu in Week 1
            wn = Math.round((ms - d.valueOf()) / (7 * 864e5)) + 1;
            return (wn < 10) ? ("0" + wn) : wn;
        },
        '%V': function(date) {
            return rule['%U'](date);
        },
        '%W': function(date) {
            return rule['%U'](date);
        },
        // Weekday as a decimal number [0,6] (0=Sunday).
        '%w': function(date) {
            return date.getDay();
        },
        // Year within century [00,99].
        '%y': function(date) {
            return '' + date.getFullYear() % 100;
        },
        // Year with century as a decimal number.
        '%Y': function(date) {
            return date.getFullYear();
        },
        '%z': function(date) {
            var minutes = new String(date.getTimezoneOffset()%60);
            var hours = new String(Math.abs(date.getTimezoneOffset()/60));
            var z = "GMT"+ (date.getTimezoneOffset() > 0 ? "-" : "+");
            hours.length < 2 ? z+="0" : "";
            z += Math.abs(date.getTimezoneOffset()/60) + ":" + date.getTimezoneOffset()%60;
            minutes.length < 2 ? z+="0" : "";
            return z;
        },
        '%Z': function(date) {
            var minutes = new String(date.getTimezoneOffset()%60);
            var hours = new String(Math.abs(date.getTimezoneOffset()/60));
            var z = (date.getTimezoneOffset() > 0 ? "-" : "+");
            hours.length < 2 ? z += "0" : "";
            z += Math.abs(date.getTimezoneOffset()/60) + "" + date.getTimezoneOffset()%60;
            minutes.length < 2 ? z += "0" : "";
        },
        '%%': function() {
            return '%';
        }
    };
    sOutput = sFormat;
    var a = sFormat.match(/%./g) || [];
    var len = a.length;
    for(var ii=0; ii<len; ii++) {
        if(rule[a[ii]]) {
            sOutput = sOutput.split(a[ii]).join(rule[a[ii]](dDate));
        }
    }
    return sOutput;
};

/**
 * Checks if date is valid
 * @param date
 */
UITools.Date.valid = function(date) {
    if ( Object.prototype.toString.call(date) !== "[object Date]" )
        return false;
    return !isNaN(date.getTime());
};





/*
EXAMPLE OF USAGE

new UITools.Transport({
    	url: 'demo.php',
    	method: 'POST',
    	postData: 'asd=123&ffr=56',
    	onLoad: function(responseText) {
    	    alert(responseText);
    	},
    	onError: function(status) {
    		alert('Server responded: ' + status);
    	}
});

*/

// initialize namespace
if (typeof SS == 'undefined') {
	SS = function() {};
}

// transport module
UITools.Transport = function(objArgs) {
	this.config = objArgs;
	var self = this;
	// check user browser and create request object
	if(window.XMLHttpRequest) {
		this.requestObj = new XMLHttpRequest(); //not for IE
	} else if (window.ActiveXObject) {
		this.requestObj = new ActiveXObject("Msxml2.XMLHTTP"); //for IE
		if (!this.requestObj) {
			this.requestObj = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	this.requestObj.onreadystatechange = function() {
		if(self.requestObj.readyState == 4) {
			var status = self.requestObj.status;
			if (status == 200) {
				self.config.onLoad(self.requestObj.responseText);
			} else if (status == 503) {
				if(confirm('Server is overloaded. Do you want to send request again ?')) {
					self.sendRequest();
				}
			} else {
				if(self.config.onError) {
					self.config.onError(status);
				}
			}
		}		
	};
	this.sendRequest();
};

// sends request to the server and executes onLoad function
UITools.Transport.prototype.sendRequest = function() {
	var self = this;
		
	if(this.config.method=='POST') {
		this.requestObj.open('POST', this.config.url, true);
		this.requestObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		this.requestObj.send(this.config.postData);
	} else {
		this.requestObj.open('GET', this.config.url, true);
		this.requestObj.send(null);
	}
};





