/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 * 
 * @fileoverview Screen module
 * 
 */

if (typeof UITools == 'undefined') {
	alert('uitools.js is required to run UITools.Screen');
}


/**
 * Constructor of the object
 * @constructor
 * @param oArgs {object} descr
 */
UITools.Screen = function(oArgs) {
	// Call superclass constructor
	UITools.Screen.superconstructor.call(this, oArgs);
};

// inherit basic functionality
UITools.inherit(UITools.Screen, UITools.Widget);

/**
 * Initialize
 */
UITools.Screen.prototype.init = function(oArgs) {
    UITools.Screen.superclass.init.call(this, oArgs);

    this.scene = [];
    this.positionCursor = null;


	var self = this;
	this.scene = UITools.getElementsByClassName('scene');
	this.autoresize();



	var aWidgets, oWidget, oScene;
	var iWidget, iScene;
	for(iScene=0; iScene<this.scene.length; iScene++) {
		oScene = this.scene[iScene];
		// Get scene widgets
		aWidgets = UITools.getElementsByClassName('widget', '*', oScene);
		for(iWidget=0; iWidget<aWidgets.length; iWidget++) {
			oWidget = aWidgets[iWidget];
			new UITools.Draggable({
				container: oWidget,
				allowResize: true,
				eventListeners: {
					mouseDown: function() {
						self.positionCursor = document.createElement('div');
						self.positionCursor.style.display = 'none';
						self.positionCursor.style.position = 'absolute';
						self.positionCursor.style.border = '1px dashed black';
						document.body.appendChild(self.positionCursor);
					},
					mouseUp: function(oMouseEvent, oOffset, oWidget) {
						if(self.positionCursor) {
							document.body.removeChild(self.positionCursor);
							self.positionCursor = null;
						}

						// move widget
						var possibleWidgetDims = self.getPossiblePosition(oOffset, oWidget);
						if(!possibleWidgetDims) {
							return;
						}
						if(!oWidget) {
							return;
						}
						oWidget.style.top = possibleWidgetDims.offset.top + 'px';
						oWidget.style.left = possibleWidgetDims.offset.left + 'px';
						oWidget.style.width = possibleWidgetDims.offset.width + 'px';
						oWidget.style.height = possibleWidgetDims.offset.height + 'px';

						self.setClassNameProperty(oWidget, 'top', possibleWidgetDims.top);
						self.setClassNameProperty(oWidget, 'left', possibleWidgetDims.left);
						self.setClassNameProperty(oWidget, 'width', possibleWidgetDims.width);
						self.setClassNameProperty(oWidget, 'height', possibleWidgetDims.height);

						var oScene = self.getHoveredScene(oOffset);
						if(oScene!=oWidget.parentNode) {
							oWidget.parentNode.removeChild(oWidget);
							oScene.appendChild(oWidget);
						}
					},
					mouseMove: function(oMouseEvent, oOffset, oWidget) {
						var possibleWidgetDims = self.getPossiblePosition(oOffset, oWidget);
						if(!possibleWidgetDims) {
							self.positionCursor.style.display = 'none';
							return;
						}
						self.positionCursor.style.display = 'block';
						self.positionCursor.style.top = possibleWidgetDims.offset.top + 'px';
						self.positionCursor.style.left = possibleWidgetDims.offset.left + 'px';
						self.positionCursor.style.width = possibleWidgetDims.offset.width + 'px';
						self.positionCursor.style.height = possibleWidgetDims.offset.height + 'px';
						// Fix width and height of the cursor if it contains border or any property
						// that expands it's dimensions
						var positionCursorOffset = UITools.getElementOffset(self.positionCursor);
						if(positionCursorOffset.width!=possibleWidgetDims.offset.width) {
							var diff = positionCursorOffset.width - possibleWidgetDims.offset.width;
							self.positionCursor.style.width = (possibleWidgetDims.offset.width - diff) + 'px';
						}
						if(positionCursorOffset.height!=possibleWidgetDims.offset.height) {
							var diff = positionCursorOffset.height - possibleWidgetDims.offset.height;
							self.positionCursor.style.height = (possibleWidgetDims.offset.height - diff) + 'px';
						}
					}
				}
			});
		}
	}
};



UITools.Screen.prototype.getHoveredScene = function(oCloneOffset) {
	var iScene, offScene;
	var iCloneCenterX = oCloneOffset.left + (oCloneOffset.width / 2);
	var iCloneCenterY = oCloneOffset.top + (oCloneOffset.height / 2);
	for(iScene=0; iScene<this.scene.length; iScene++) {
		offScene = UITools.getElementOffset(this.scene[iScene]);
		if(offScene.left < iCloneCenterX && (offScene.left + offScene.width) > iCloneCenterX &&
			offScene.top < iCloneCenterY && (offScene.top + offScene.height) > iCloneCenterY) {
			return this.scene[iScene];
		}
	}
	return null;
};

UITools.Screen.prototype.getPossiblePosition = function(oCloneOffset, oOrigWidget) {
	var oScene = this.getHoveredScene(oCloneOffset);
	if(!oScene) {
		return;
	}
	var sceneProps = this.getSceneProperties(oScene);
	var possibleWidgetDims = {};
	// Get possible widget dimensions for the new scene
	possibleWidgetDims.width = Math.round(oCloneOffset.width / sceneProps.cellWidth);
	if(possibleWidgetDims.width<1) {
		possibleWidgetDims.width = 1;
	}
	possibleWidgetDims.height = Math.round(oCloneOffset.height / sceneProps.cellHeight);
	if(possibleWidgetDims.height<1) {
		possibleWidgetDims.height = 1;
	}
	possibleWidgetDims.top = Math.round((oCloneOffset.top-sceneProps.offset.top) / sceneProps.cellHeight);
	possibleWidgetDims.left = Math.round((oCloneOffset.left-sceneProps.offset.left) / sceneProps.cellWidth);
	possibleWidgetDims.offset = {
		top: Math.round((possibleWidgetDims.top * sceneProps.cellHeight) + sceneProps.offset.top),
		left: Math.round((possibleWidgetDims.left * sceneProps.cellWidth) + sceneProps.offset.left),
		width: possibleWidgetDims.width * sceneProps.cellWidth,
		height: possibleWidgetDims.height * sceneProps.cellHeight
	};
	// Check if position is inside the scene
	if(possibleWidgetDims.top < 0 || possibleWidgetDims.left < 0
			|| possibleWidgetDims.top + possibleWidgetDims.height > sceneProps.height
			|| possibleWidgetDims.left + possibleWidgetDims.width > sceneProps.width
			) {
		return null;
	}
	// Seach widget interceptions
	// Return null if found
	var aWidgets = this.getSceneWidgets(oScene);
	var iWidget, oWidget, widgetProps;
	var a = {
		y: possibleWidgetDims.top,
		x: possibleWidgetDims.left
	};
	var b = {
		y: possibleWidgetDims.top,
		x: possibleWidgetDims.left + possibleWidgetDims.width - 1
	};
	var c = {
		y: possibleWidgetDims.top + possibleWidgetDims.height - 1,
		x: possibleWidgetDims.left + possibleWidgetDims.width - 1
	};
	var d = {
		y: possibleWidgetDims.top + possibleWidgetDims.height - 1,
		x: possibleWidgetDims.left
	};
	var iWidgetLength = aWidgets.length;
	for(iWidget=0; iWidget<iWidgetLength; iWidget++) {
		oWidget = aWidgets[iWidget];
		if(oWidget==oOrigWidget) {
			continue;
		}
		widgetProps = this.getWidgetProperties(oWidget);
		if(!widgetProps) {
			continue;
		}
//		console.log(widgetProps.top +' <= '+a.y+' && '+widgetProps.top+'+'+widgetProps.height+' >= '+a.y+'\
//				&& '+widgetProps.left+' <= '+a.x+' && '+widgetProps.left+'+'+widgetProps.width+' >= '+a.x);
//		console.log(a.y);
		if(
				widgetProps.top <= a.y && (widgetProps.top+widgetProps.height-1) >= a.y
				&& widgetProps.left <= a.x && (widgetProps.left+widgetProps.width-1) >= a.x
				||
				widgetProps.top <= b.y && (widgetProps.top+widgetProps.height-1) >= b.y
				&& widgetProps.left <= b.x && (widgetProps.left+widgetProps.width-1) >= b.x
				||
				widgetProps.top <= c.y && (widgetProps.top+widgetProps.height-1) >= c.y
				&& widgetProps.left <= c.x && (widgetProps.left+widgetProps.width-1) >= c.x
				||
				widgetProps.top <= d.y && (widgetProps.top+widgetProps.height-1) >= d.y
				&& widgetProps.left <= d.x && (widgetProps.left+widgetProps.width-1) >= d.x
				||

				a.y<=widgetProps.top && d.y>=widgetProps.top
				&& a.x<=widgetProps.left && b.x>=widgetProps.left
				||
				a.y<=widgetProps.top && d.y>=widgetProps.top
				&& a.x<=widgetProps.left+widgetProps.width-1 && b.x>=widgetProps.left+widgetProps.width-1
				||
				a.y<=widgetProps.top+widgetProps.height-1 && d.y>=widgetProps.top+widgetProps.height-1
				&& a.x<=widgetProps.left+widgetProps.width-1 && b.x>=widgetProps.left+widgetProps.width-1
				||
				a.y<=widgetProps.top+widgetProps.height-1 && d.y>=widgetProps.top+widgetProps.height-1
				&& a.x<=widgetProps.left && b.x>=widgetProps.left

				||
		        widgetProps.top>=a.y && widgetProps.left<=a.x && widgetProps.left+widgetProps.width-1>=a.x
		        && widgetProps.top>=b.y && widgetProps.left<=b.x && widgetProps.left+widgetProps.width-1>=b.x
				&& widgetProps.top+widgetProps.height-1<=c.y && widgetProps.left<=c.x && widgetProps.left+widgetProps.width-1>=c.x
				&& widgetProps.top+widgetProps.height-1<=d.y && widgetProps.left<=d.x && widgetProps.left+widgetProps.width-1>=d.x
				||
				widgetProps.top<=a.y && widgetProps.top+widgetProps.height-1>=a.y && widgetProps.left>=a.x
				&& widgetProps.top<=d.y && widgetProps.top+widgetProps.height-1>=d.y && widgetProps.left>=d.x
				&& widgetProps.top<=b.y && widgetProps.top+widgetProps.height-1>=b.y && widgetProps.left+widgetProps.width-1<=b.x

				) {
			return null;
		}
	}

	return possibleWidgetDims;
};

UITools.Screen.prototype.getClassNameProperty = function(oElement, sProperty) {
	if(!oElement) {
		return;
	}
	var result;
	var classNames = oElement.className.split(' ');
	var reg = new RegExp("^" + sProperty + "=(.*)$");
	for(var ii=0; ii<classNames.length; ii++) {
		result = reg.exec(classNames[ii]);
		if(result) {
			return result[1];
		}
	}
	return null;
};

UITools.Screen.prototype.setClassNameProperty = function(oElement, sProperty, sValue) {
	if(!oElement) {
		return;
	}
	var result;
	var classNames = oElement.className.split(' ');
	var reg = new RegExp("^(" + sProperty + ")=.*$");
	for(var ii=0; ii<classNames.length; ii++) {
		result = reg.exec(classNames[ii]);
		if(result) {
			classNames[ii] = sProperty + '=' + sValue;
			oElement.className = classNames.join(' ');
			return;
		}
	}
	classNames.push(sProperty + '=' + sValue);
	oElement.className = classNames.join(' ');		
};

UITools.Screen.prototype.getSceneProperties = function(oScene) {
	if(!oScene) {
		return;
	}
	var screenProps = {};
	// Get scene offset
	screenProps.offset = UITools.getElementOffset(oScene);
	// Get scene dimensions
	screenProps.width = this.getClassNameProperty(oScene, 'width');
	screenProps.height = this.getClassNameProperty(oScene, 'height');
	// Get cell dimensions
	screenProps.cellWidth = screenProps.offset.width / screenProps.width;
	screenProps.cellHeight = screenProps.offset.height / screenProps.height;
	
	return screenProps;	
};

UITools.Screen.prototype.getWidgetProperties = function(oWidget) {
	var widgetProps = {};
	widgetProps.offset = UITools.getElementOffset(oWidget);
	widgetProps.width = parseInt(this.getClassNameProperty(oWidget, 'width'));
	widgetProps.height = parseInt(this.getClassNameProperty(oWidget, 'height'));
	widgetProps.top = parseInt(this.getClassNameProperty(oWidget, 'top'));
	widgetProps.left = parseInt(this.getClassNameProperty(oWidget, 'left'));
	if(!widgetProps.width) {
		widgetProps.width = 1;
	}
	if(!widgetProps.height) {
		widgetProps.height = 1;
	}
	return widgetProps;
};

UITools.Screen.prototype.getSceneWidgets = function(oScene) {
	return UITools.getElementsByClassName('widget', '*', oScene);	
};

UITools.Screen.prototype.autoresize = function() {
	var aWidgets, oWidget, oScene, sceneProps, widgetProps, iWidget, iScene;
	var iWidgetWidth, iWidgetHeight, iWidgetTop, iWidgetLeft;
	for(iScene=0; iScene<this.scene.length; iScene++) {
		oScene = this.scene[iScene];
		sceneProps = this.getSceneProperties(oScene);
		// Get scene widgets
		aWidgets = this.getSceneWidgets(oScene);
		for(iWidget=0; iWidget<aWidgets.length; iWidget++) {
			oWidget = aWidgets[iWidget];
			widgetProps = this.getWidgetProperties(oWidget);
			oWidget.style.width = Math.ceil(sceneProps.cellWidth * widgetProps.width) + 'px';
			oWidget.style.height = Math.ceil(sceneProps.cellHeight * widgetProps.height) + 'px';
			oWidget.style.position = 'absolute';
			oWidget.style.top = Math.ceil(sceneProps.offset.top + sceneProps.cellHeight * widgetProps.top) + 'px';
			oWidget.style.left = Math.ceil(sceneProps.offset.left + sceneProps.cellWidth * widgetProps.left) + 'px';
		}
	}
};