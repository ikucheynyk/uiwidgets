/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 * 
 * @fileoverview Preloader module
 * 
 */

// initialize namespace
if (typeof SS == 'undefined') {
	SS = function() {};
}

/**
 * Constructor of the object
 * @constructor
 * @param oArgs {object} descr
 */
UITools.Preloader = function(oArgs) {
	// Call superclass constructor
	UITools.Preloader.superconstructor.call(this, oArgs);
};


// inherit basic functionality
UITools.inherit(UITools.Preloader, UITools.Widget);


UITools.Preloader.prototype.init = function(oArgs) {
    UITools.Preloader.superclass.init.call(this, oArgs);

	this.media = oArgs.media;
	this.imageElems = [];
	this.processedImages = 0;
	this.progress = 0;
	this.title = this.config.title ? this.config.title : 'Loading...';

    // HTML layout
	this.container = document.createElement('DIV');
	document.body.appendChild(this.container);
	this.container.className = 'UITPreloader_' + this.theme;
	this.container.innerHTML = '<div class="Shadow">'
		+ '<div class="Panel">'
			+ '<div class="Inner">'
				+ '<div class="Title">' + this.title + '</div>'
				+ '<div class="Progress" id="UITPreloader' + this.objectId + 'ProgressContainer">'
					+ '<div class="ProgressBar" id="UITPreloader' + this.objectId + 'ProgressBar"></div>'
				+ '</div>'
				+ '<div class="Percentage" id="UITPreloader' + this.objectId + 'Percentage">0%</div>'
			+ '</div>'
		+ '</div>'
	+ '</div>';
	this.shadow = this.container.firstChild;
	this.panel = this.container.firstChild.firstChild;
	this.progressContainer = document.getElementById('UITPreloader' + this.objectId + 'ProgressContainer');
	this.progressBar = document.getElementById('UITPreloader' + this.objectId + 'ProgressBar');
	this.percentage = document.getElementById('UITPreloader' + this.objectId + 'Percentage');
	// Preloader must be centered
	var oPreloaderOffset = UITools.getElementOffset(this.panel);
	this.autoresize();
	// Element size must be allways the same as body size
	// Set resize event
    var self = this;
	window.addEventListener('resize', function() {
		self.autoresize()
	}, false);
	setTimeout(function() {
		self.preloadOneByOne(0);
	}, 60);
};


/**
 * Resizes screensaver container according to body size
 */
UITools.Preloader.prototype.autoresize = function() {
	var self = this;
	// Get browser (visual area) and window container dimensions
	var areaDims = UITools.getWindowDimensions();
	var panelOffset = UITools.getElementOffset(this.panel);
	// Set shadow container to overlap whole page
	this.shadow.style.width = areaDims.width + 'px';
	this.shadow.style.height = areaDims.height + 'px';
	// Center the window
	var iLeft = Math.floor((areaDims.width/2) - (panelOffset.width/2));
	var iTop = Math.floor((areaDims.height/2) - (panelOffset.height/2));
	this.panel.style.top = iTop + 'px';
	this.panel.style.left = iLeft + 'px';
};

/**
 * Preloads the image
 * @private
 */
UITools.Preloader.prototype.preloadOneByOne = function(iMedia) {
	var self = this;
	var onLoad = function() {
		self.processedImages++;
		self.updateProgress();
		if(self.media.length == self.processedImages) {
			setTimeout(function() {
				self.complete();
			}, 60);			
			return;
		}
		setTimeout(function() {
			self.preloadOneByOne(iMedia+1);
		}, 20);
	};
	// Creane an image
	this.imageElems[iMedia] = new Image(20,20);
	this.imageElems[iMedia].addEventListener('load', onLoad, false);
	this.imageElems[iMedia].addEventListener('error', onLoad, false);
	this.imageElems[iMedia].src = this.media[iMedia];
};

/**
 * Updates the progress bar
 * @private
 */
UITools.Preloader.prototype.updateProgress = function() {
	// Get percentage value
	var fProgress = (100 / this.media.length) * this.processedImages; // float percentage
	this.progress = Math.floor(fProgress); // Kep integer percentage
	this.percentage.innerHTML = this.progress+'%';
	var oProgressContainerOffset = UITools.getElementOffset(this.progressContainer);
	var iPixProgress = Math.floor((oProgressContainerOffset.width / 100) * fProgress);
	this.progressBar.style.width = iPixProgress + 'px';
};

/**
 * Remove loading screen
 * @private
 */
UITools.Preloader.prototype.complete = function() {
	if(this.config.onLoad) {
		this.config.onLoad();
	}
	this.container.style.display = 'none';
};