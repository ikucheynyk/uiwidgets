/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 * 
 * @fileoverview Window module
 * 
 */

if (typeof UITools == 'undefined') {
	alert('uitools.js is required to run UITools.Window');
}

/**
 * Constructor of the object
 * @constructor
 * @param oArgs {object} descr
 */
UITools.Window = function(oArgs) {
	// Call superclass constructor
	UITools.Window.superconstructor.call(this, oArgs);
};

// inherit basic functionality
UITools.inherit(UITools.Window, UITools.Widget);


UITools.Window.prototype.init = function(oArgs) {
    UITools.Window.superclass.init.call(this, oArgs);
    
	var self = this;
    this.delay = oArgs.delay ? oArgs && oArgs.delay : 10000;
    this.mode = oArgs.mode ? oArgs && oArgs.mode : 'quick';
    this.initialized = false;
	// Create element
	this.container = document.createElement('DIV');
	this.container.style.display = 'none';
	this.container.id = 'UITWindow' + this.objectId + 'Container';
	this.container.className = 'UITWindow_' + this.theme;
	document.body.appendChild(this.container);
	this.container.innerHTML = '<div id="UITWindow' + this.objectId + 'Shadow" class="shadow"></div>'
		+ '<div id="UITWindow' + this.objectId + 'WindowContainer" class="WindowContainer">'
		+ '<table cellpadding="0" cellspacing="0" class="WindowFrame">'
			+ '<tbody>'
				+ '<tr>'
						+ '<td class="LeftTop"></td>'
						+ '<td class="Top">'
							+ '<div id="UITWindow' + this.objectId + 'Title" class="Title"></div>'
							+ '<div class="Buttons">'
								+ '<div class="ButtonClose" onclick="UITools.objects[' + this.objectId + '].close()"></div>'
							+ '</div>'
						+ '</td>'
						+ '<td class="RightTop"></td>'
				+ '</tr><tr>'
						+ '<td class="Left"></td>'
						+ '<td class="Content"><div class="ContentContainer" id="UITWindow' + this.objectId + 'ContentContainer"></div></td>'
						+ '<td class="Right"></td>'
				+ '</tr><tr>'
						+ '<td class="LeftBottom"></td>'
						+ '<td class="Bottom"></td>'
						+ '<td class="RightBottom"></td>'
				+ '</tr>'
			+ '</tbody>'
		+ '</table>'
		+ '</div>';
	var winDims = UITools.getWindowDimensions();
	this.shadowContainer = this.container.firstChild;
	this.windowContainer = this.container.lastChild;
	if(this.config.width) {
		this.windowContainer.firstChild.style.width = this.config.width + 'px';
	}
	if(this.config.height) {
		this.windowContainer.firstChild.style.height = this.config.height + 'px';
	}
	this.windowTitle = document.getElementById('UITWindow' + this.objectId + 'Title');
	this.windowContentContainer = document.getElementById('UITWindow' + this.objectId + 'ContentContainer');
	
	this.shadowContainer.style.backgrounColor = '#000000';
	this.shadowContainer.style.position = 'fixed';	
	this.shadowContainer.style.width = winDims.width + 'px';
	this.shadowContainer.style.height = winDims.height + 'px';
	this.shadowContainer.style.top = '0px';
	this.shadowContainer.style.left = '0px';
	this.shadowContainer.style.zIndex = 100;
	// Element size must be allways the same as body size
	// Set resize event
	UITools.addEvent(window, 'resize', function() {
		self.autoresize()
	}, false);
	this.initialized = true;
};

/**
 * Displays the Container softly using opacity property
 * @private
 * @param fCallback {function} Callback function is called when fading done
 * @param iOpacity {number} Opacity value (number between 0 and 10)
 */
UITools.Window.prototype.fadeIn = function(oElem, fCallback, iOpacity) {
	var self = this;
	oElem.style.opacity = iOpacity/10;
	if(iOpacity>=10) {
		// Run callback if specified
		if(fCallback) {
			fCallback();
		}
		return;
	}
	setTimeout(function() {
		self.fadeIn(oElem, fCallback, iOpacity+0.1);
	}, 20)
};

/**
 * Removes the Container softly using opacity property
 * @private
 * @param fCallback {function} Callback function is called when fading done
 * @param iOpacity {number} Opacity value (number between 0 and 10)
 */
UITools.Window.prototype.fadeOut = function(oElem, fCallback, iOpacity) {
	var self = this;
	oElem.style.opacity = iOpacity/10;
	if(iOpacity<=0) {
		// Run callback if specified
		if(fCallback) {
			fCallback();
		}
		return;
	}
	setTimeout(function() {
		self.fadeOut(oElem, fCallback, iOpacity-0.1);
	}, 20)
};

/**
 * Resizes screensaver container according to body size
 */
UITools.Window.prototype.autoresize = function() {
	var self = this;
	// Get browser (visual area) and window container dimensions
	var areaDims = UITools.getWindowSize();
	var bodyOffset = UITools.getElementOffset(document.body);

	var winOffset = UITools.getElementOffset(this.windowContainer);
	// Set shadow container to overlap whole page
	this.shadowContainer.style.width = '100%';
	this.shadowContainer.style.height = '100%';
	// Center the window
	var iLeft = Math.floor((areaDims.width/2) - (winOffset.width/2));
	var iTop = Math.floor((areaDims.height/2) - (winOffset.height/2));
	this.windowContainer.style.top = iTop + 'px';
	this.windowContainer.style.left = iLeft + 'px';
};

/**
 * Opens the window
 */
UITools.Window.prototype.open = function(oArgs) {
	var self = this;
	if(!this.initialized) {
		this.init();
	}
	if(oArgs.width) {
		this.windowContentContainer.style.width = oArgs.width + 'px';
	} else {
		this.windowContentContainer.style.width = 'auto';
	}
	if(oArgs.height) {
		this.windowContentContainer.style.height = oArgs.height + 'px';
	} else {
		this.windowContentContainer.style.width = 'auto';
	}
	if(oArgs.title) {
		this.windowTitle.innerHTML = oArgs.title;
	}
	if(oArgs.source && oArgs.sourceType=='html/text') {
		this.windowContentContainer.innerHTML = oArgs.source;
	} else if(oArgs.source && oArgs.sourceType=='html/url') {
		this.windowContentContainer.innerHTML = '<iframe class="Frame" src="' + oArgs.source + '"></iframe>';
	}
	this.container.style.display = 'block';
	this.autoresize();
	//this.fadeIn(this.container, null, 0);
	
    // added by Andrew
    if(oArgs.source && oArgs.sourceType=='html/text') {
	   this.runScripts(this.windowContentContainer);
    }
};

/**
 * Closes the window
 * @public
 * @param oArgs {json} Arguments
 */
UITools.Window.prototype.close = function() {
	var self = this;
	self.container.style.display = 'none';
	/*
	this.fadeOut(this.container, function() {
		self.container.style.display = 'none';
	}, 10);
	*/
};

/**
 * Search script nodes and execute code in them
 * @private
 * @param oContentContainer {element} Html element
 */
UITools.Window.prototype.runScripts = function(oContentContainer) {
	var oNode;
	// Go throught nodes and search script nodes
	for(var ii=0; ii<oContentContainer.childNodes.length; ii++) {
		oNode = oContentContainer.childNodes[ii];
		if(oNode && oNode.tagName=='SCRIPT') {
			// Just execute them
			eval(oNode.innerHTML);
		}
	}
};
