/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 * 
 * @fileoverview Javascript tree module
 * 
 * Example of usage
 * 
 * <pre>
 * 		tr1 = new UITools.Tree({
 * 			container: document.getElementById('tree1'),
 * 			//sourceType: 'json',
 * 			source: [{
 * 				label: 'Node Title 1',
 * 				expanded: true,
 * 				children: [{
 * 					label: 'Subnode 1'
 * 				},{
 * 					label: 'Subnode 2',
 * 					expanded: true,
 * 					children: [{
 * 						label: 'Subnode'
 * 					}]
 * 				},{
 * 					label: 'Subnode 3',
 * 					expanded: true,
 * 					sourceType: 'json/url',
 * 					source: 'recursive_branch.php'
 * 				},{
 * 					label: 'Subnode 4',
 * 					children: [{
 * 						label: 'asd',
 * 						children: [{
 * 							label: '123'
 * 						}]
 * 					}]
 * 				}]					
 * 			},{
 * 				label: '123'
 * 			}]
 * 		});		
 * </pre> 
 * 
 * Config Options:
 * <b>container</b>
 * <b>sourceType</b>
 * <b>source</b>
 * <b>selectOnLabelClick</b>
 * <b>expandOnLabelClick</b>
 * <b>expandCollapseOnLabelClick</b>
 * <b>expandAll</b>
 * 
 * Eventlisteners:
 * <b>iconClick</b>
 * <b>signClick</b>
 * <b>labelClick</b>
 * <b>select</b>
 * <b>expand</b>
 * <b>collapse</b>
 * 
 */

if (typeof UITools == 'undefined') {
	alert('uitools.js is required to run UITools.Tree');
}

/**
 * Constructor of the tree object
 * @constructor
 * @private
 * @param oArgs {object} Object configuration
 */
UITools.Tree = function(oArgs) {
	// Call superclass constructor
	UITools.Tree.superconstructor.call(this, oArgs);
};

// inherit basic functionality
UITools.inherit(UITools.Tree, UITools.Widget);

UITools.Tree.prototype.init = function(oArgs) {
    UITools.Tree.superclass.init.call(this, oArgs);
    
    // Container is required
    this.container = this.element(oArgs.container);
    if(!this.isElement(this.container)) {
        alert('UITools.Tree init failed: container missing or invalid');
    }
    // Source type, can be json, json/url, html
	this.sourceType = this.config.sourceType;
	this.source = this.config.source;

	// Create tree table
	// generate html
	this.container.innerHTML = '<TABLE class="rootNode_' + this.theme + '" cellpadding="0" cellspacing="0"><TBODY></TBODY></TABLE>';
	this.tableChildren = this.container.firstChild;

	this.childrenInitialized = false;

    this.nodeType = 'root';
    this.children = [];
    this.allNodes = [];

	// Init
	if(this.sourceType=='json/url' && this.source) {
		this.preload();
	} else {
		// init children
		for(var ii=0; ii<this.config.source.length; ii++) {
			this.addChild(this.config.source[ii]);
		}
		this.childrenInitialized = true;
		// fire event `init`
        this.fireEvent('init', {
            tree: this
        });
	}
};



/**
 * Creates new child node
 * @param oArgs {object} New child json object
 * @return Created node object
 */
UITools.Tree.prototype.addChild = function(oArgs) {
	// create node
	var newNode = new UITools.TreeNode(oArgs);
	// create links
	newNode.parent = this;
	newNode.rootNode = this;
	// generate row elements
	newNode.render(this.tableChildren);
	// add node to children
	this.children.push(newNode);
	// add node to all nodes array
	this.allNodes.push(newNode);
	// init children
	if(oArgs.children) {
		for(var ii=0; ii<oArgs.children.length; ii++) {
			newNode.addChild(oArgs.children[ii]);
		}
	}
	// expand if needed
	if(newNode.expanded || this.config.expandAll) {
		newNode.expand();
	}
	// update first & last child and update style classes
	this.resetFirstLastChild();	
}

/**
 * Set theme classes for node elements
 * @private
 */
UITools.Tree.prototype.resetFirstLastChild = function() {
	// reset first and last child classes
	if(this.lastChild) {
		if(this.lastChild.childrenInitialized && this.lastChild.children.length==0) {
			this.lastChild.rowLabel.className = 'middleChild Empty';
		} else {
			this.lastChild.rowLabel.className = 'middleChild ' + (this.lastChild.expanded ? 'Expanded' : 'Collapsed');
		}
		this.lastChild.rowChildren.className = 'middleChildChildren';
		this.lastChild.cellLine.className = 'middleChildChildrenCellLine';
	}	
	if(this.firstChild) {
		if(this.firstChild.childrenInitialized && this.firstChild.children.length==0) {
			this.firstChild.rowLabel.className = 'middleChild Empty';
		} else {
			this.firstChild.rowLabel.className = 'middleChild ' + (this.firstChild.expanded ? 'Expanded' : 'Collapsed');
		}
		this.firstChild.rowChildren.className = 'middleChildChildren';
		this.firstChild.cellLine.className = 'middleChildChildrenCellLine';
	}	
	// update links to first and last child node objects
	if(this.children.length) {
		this.firstChild = this.children[0];
		this.lastChild = this.children[this.children.length-1];
		// if there is only 1 child
		if(this.children.length==1) {
			// set only
			if(this.firstChild.childrenInitialized && this.firstChild.children.length==0) {
				this.firstChild.rowLabel.className = 'onlyChild Empty';
			} else {
				this.firstChild.rowLabel.className = 'onlyChild ' + (this.firstChild.expanded ? 'Expanded' : 'Collapsed');
			}
			this.firstChild.rowChildren.className = 'onlyChildChildren';
			this.firstChild.cellLine.className = 'onlyChildChildrenCellLine';
		} else {
			// set first
			if(this.firstChild.childrenInitialized && this.firstChild.children.length==0) {
				this.firstChild.rowLabel.className = 'firstChild Empty';
			} else {
				this.firstChild.rowLabel.className = 'firstChild ' + (this.firstChild.expanded ? 'Expanded' : 'Collapsed');
			}
			this.firstChild.rowChildren.className = 'firstChildChildren';
			this.firstChild.cellLine.className = 'firstChildChildrenCellLine';
			// set last
			if(this.lastChild.childrenInitialized && this.lastChild.children.length==0) {
				this.lastChild.rowLabel.className = 'lastChild Empty';
			} else {
				this.lastChild.rowLabel.className = 'lastChild ' + (this.firstChild.expanded ? 'Expanded' : 'Collapsed');
			}
			this.lastChild.rowChildren.className = 'lastChildChildren';
			this.lastChild.cellLine.className = 'lastChildChildrenCellLine';
		}
	} else {
		this.firstChild = null;
		this.lastChild = null;
	}
};

/**
 * Preload node children 
 * @return Returns false if node doesn't have source to preload or sourceType is not url type
 */
UITools.Tree.prototype.preload = function(onPreload) {
	var self = this;
	
	if(this.sourceType!='json/url' || !this.source) {
		return false;
	}
	
	if(!UITools.Transport) {
		alert('Preloading requires transport module to be loaded');
		return false;
	}
		
	// preload children
	new UITools.Transport({
	   	url: self.source,
   		method: 'GET',
   		onLoad: function(oResponse) {
			oResponse = UITools.jsonDecode(oResponse);
			if(oResponse) {
				for(var ii=0; ii<oResponse.length; ii++) {
					self.addChild(oResponse[ii]);
				}
				self.childrenInitialized = true;
				// Run callback
				if(onPreload) {
					onPreload(self);
				}
				// fire event `init`
                self.fireEvent('init', {
                    tree: self
                });
			} else {
				alert('Incorrect JSON');
			}
   		},
		onError: function() {
			alert('Error');				
		}
	});
	
	return true;	
};

/**
 * Expand all nodes in the tree
 */
UITools.Tree.prototype.expandAll = function() {
	for(var ii=0; ii<this.allNodes.length; ii++) {
		this.allNodes[ii].expand();
	}
};

/**
 * Collapse all nodes in the tree
 */
UITools.Tree.prototype.collapseAll = function() {
	for(var ii=0; ii<this.allNodes.length; ii++) {
		this.allNodes[ii].collapse();
	}
};

/**
 * Constructor of the tree node object
 * @constructor
 * @private
 * @param oArgs {object} Object configuration
 */
UITools.TreeNode = function(oArgs) {
	// Call superclass constructor
	UITools.TreeNode.superconstructor.call(this, oArgs);
};

// inherit basic functionality
UITools.inherit(UITools.TreeNode, UITools.Widget);

UITools.TreeNode.prototype.init = function(oArgs) {
    UITools.TreeNode.superclass.init.call(this, oArgs);

    this.config = oArgs;
    this.nodeType = 'child';
    this.sourceType = this.config.sourceType;
    this.source = this.config.source;

    // Init
    this.label = oArgs.label;
    this.expanded = oArgs.expanded ? oArgs.expanded : false;
    this.children = [];

    this.childrenInitialized = !(this.sourceType == 'json/url' && this.source);

};

/**
 * Creates new child node
 * @param oArgs {object} New child json object
 * @return Created node object
 */
UITools.TreeNode.prototype.addChild = function(oArgs) {
	// create new node
	var newNode = new UITools.TreeNode(oArgs);
	// create links
	newNode.parent = this;
	newNode.rootNode = this.rootNode;
	// generate row elements
	newNode.render(this.tableChildren);
	// add to children
	this.children.push(newNode);	
	// add node to all nodes array
	this.rootNode.allNodes.push(newNode);	
	// init children
	if(oArgs.children) {
		for(var ii=0; ii<oArgs.children.length; ii++) {
			newNode.addChild(oArgs.children[ii]);
		}
	}
	// expand if needed
	if(newNode.expanded) {
		newNode.expand();
	}		
	// update first & last child and update style classes
	this.resetFirstLastChild();
	// return created node
	return newNode;
};

/**
 * Generate node html elements, set default theme classes
 * create links to elements inside node object
 * @private
 */
UITools.TreeNode.prototype.render = function(tableCurrent) {
	var self = this;
	if(!tableCurrent) {
		alert('Current table invalid');
		return false;
	}
	
	this.tableCurrent = tableCurrent;
	
	// row with label and sign
	var cellSign = document.createElement('TD');
	var nodeSign = document.createElement('DIV');
	cellSign.appendChild(nodeSign);
	var cellLabel = document.createElement('TD');
	var nodeLabel = document.createElement('DIV');
	var nodeIcon = document.createElement('DIV');
	nodeLabel.className = 'nodeLabel';
	nodeIcon.className = 'nodeIcon';
	
	//for FF
	nodeIcon.setAttribute("style","float:left");
	nodeLabel.setAttribute("style","float:left");
	cellLabel.setAttribute("style","float:left");
	// for IE
	nodeIcon.style.styleFloat = "left";
	nodeLabel.style.styleFloat = "left";
	cellLabel.style.styleFloat = "left";
		
	cellLabel.appendChild(nodeIcon);
	cellLabel.appendChild(nodeLabel);
	var rowLabel = document.createElement('TR');
	rowLabel.appendChild(cellSign);
	rowLabel.appendChild(cellLabel);
	
	// row with children
	var cellLine = document.createElement('TD');
	var cellChildren = document.createElement('TD');
	var rowChildren = document.createElement('TR');
	rowChildren.appendChild(cellLine);
	rowChildren.appendChild(cellChildren);
		
	// create children table
	cellChildren.innerHTML = '<TABLE class="childNode_' + this.rootNode.theme + '" cellpadding="0" cellspacing="0"><TBODY></TBODY></TABLE>';
	var tableChildren = cellChildren.firstChild;
	
	// set label
	nodeLabel.innerHTML = this.label;
	// set icon if needed
	if(this.config.iconCollapsed) {
		nodeIcon.innerHTML = '<IMG src="' + this.config.iconCollapsed + '">';
	}

	// keep links in the node
	this.tableChildren = tableChildren;
	this.rowChildren = rowChildren;
	this.rowLabel = rowLabel;
	this.cellChildren = cellChildren;
	this.cellLine = cellLine;
	this.cellSign = cellSign;
	this.cellLabel = cellLabel;
	this.nodeLabel = nodeLabel;
	this.nodeIcon = nodeIcon;
	this.nodeSign = nodeSign;
	
	tableCurrent.lastChild.appendChild(rowLabel);
	tableCurrent.lastChild.appendChild(rowChildren);
	
	// set events
	// set +- click evenet
	cellSign.onclick = function() {
		UITools.objects[self.objectId].onSignClick();
	};
	// set label click event
	nodeLabel.onclick = function() {
		UITools.objects[self.objectId].onLabelClick();
    };
	// set icon click event
	nodeIcon.onclick = function() {
		UITools.objects[self.objectId].onIconClick();
	};
	
	this.tableChildren.setAttribute('cellpadding', 0);
	this.tableChildren.setAttribute('cellspacing', 0);
	this.tableChildren.setAttribute('border', 0);
	this.tableChildren.style.margin = '0px';
	
	// set class names
	this.cellSign.className = 'nodeSignCell';
	this.nodeSign.className = 'nodeSign';
	this.cellChildren.className = 'cellChildren';
	this.cellLine.className = 'middleChildChildrenCellLine';
	this.rowLabel.className = 'middleChild' + (this.expanded ? 'Expanded' : 'Collapsed');
	this.rowChildren.className = 'middleChildChildren';
	
	// set child visibility
	if(this.expanded) {
		rowChildren.style.display = "";				
	} else {
		rowChildren.style.display = "none";
	}
	
	this.rendered = true;
	
	return true;		
}

/**
 * Set theme classes for node elements
 * @private
 */
UITools.TreeNode.prototype.resetFirstLastChild = function() {
	// reset first and last child classes
	if(this.lastChild) {
		if(this.lastChild.childrenInitialized && this.lastChild.children.length==0) {
			this.lastChild.rowLabel.className = 'middleChild Empty';
		} else {
			this.lastChild.rowLabel.className = 'middleChild ' + (this.lastChild.expanded ? 'Expanded' : 'Collapsed');
		}
		this.lastChild.rowChildren.className = 'middleChildChildren';
		this.lastChild.cellLine.className = 'middleChildChildrenCellLine';
	}	
	if(this.firstChild) {
		if(this.firstChild.childrenInitialized && this.firstChild.children.length==0) {
			this.firstChild.rowLabel.className = 'middleChild Empty';
		} else {
			this.firstChild.rowLabel.className = 'middleChild ' + (this.firstChild.expanded ? 'Expanded' : 'Collapsed');
		}
		this.firstChild.rowChildren.className = 'middleChildChildren';
		this.firstChild.cellLine.className = 'middleChildChildrenCellLine';
	}	
	// update links to first and last child node objects
	if(this.children.length) {
		this.firstChild = this.children[0];
		this.lastChild = this.children[this.children.length-1];
		// if there is only 1 child
		if(this.children.length==1) {
			// set only
			if(this.firstChild.childrenInitialized && this.firstChild.children.length==0) {
				this.firstChild.rowLabel.className = 'onlyChild Empty';
			} else {
				this.firstChild.rowLabel.className = 'onlyChild ' + (this.firstChild.expanded ? 'Expanded' : 'Collapsed');
			}
			this.firstChild.rowChildren.className = 'onlyChildChildren';
			this.firstChild.cellLine.className = 'onlyChildChildrenCellLine';
		} else {
			// set first
			if(this.firstChild.childrenInitialized && this.firstChild.children.length==0) {
				this.firstChild.rowLabel.className = 'firstChild Empty';
			} else {
				this.firstChild.rowLabel.className = 'firstChild ' + (this.firstChild.expanded ? 'Expanded' : 'Collapsed');
			}
			this.firstChild.rowChildren.className = 'firstChildChildren';
			this.firstChild.cellLine.className = 'firstChildChildrenCellLine';
			// set last
			if(this.lastChild.childrenInitialized && this.lastChild.children.length==0) {
				this.lastChild.rowLabel.className = 'lastChild Empty';
			} else {
				this.lastChild.rowLabel.className = 'lastChild ' + (this.firstChild.expanded ? 'Expanded' : 'Collapsed');
			}
			this.lastChild.rowChildren.className = 'lastChildChildren';
			this.lastChild.cellLine.className = 'lastChildChildrenCellLine';
		}
	} else {
		this.firstChild = null;
		this.lastChild = null;
	}
};

/**
 * Function called when nodeSign (+ or -) has been clicked
 * @private
 */
UITools.TreeNode.prototype.onSignClick = function() {
	if(this.expanded) {
		this.collapse();
	} else {
		this.expand();
	}
	// execute event
    this.rootNode.fireEvent('signClick', {
        node: this
    });
};

/**
 * Function called when label has been clicked
 * @private
 */
UITools.TreeNode.prototype.onLabelClick = function() {
	// if expand allowed
	if(this.rootNode.config.expandOnLabelClick) {
		// do expand
		this.expand();
	} else if(this.rootNode.config.expandCollapseOnLabelClick) {
		// expand if node collapsed, otherwise collapse
		if(this.expanded) {
			this.collapse();
		} else {
			this.expand();
		}
	}	
	// if selecting allowed
	if(this.rootNode.config.selectOnLabelClick) {
		// do select
		this.markSelected();
	}
	// call event
    this.rootNode.fireEvent('labelClick', {
        node: this
    });
};

/**
 * Function called when icon has been clicked
 * @private
 */
UITools.TreeNode.prototype.onIconClick = function() {
	// call event
    this.rootNode.fireEvent('iconClick', {
        node: this
    });
};

/**
 * Marks node as selected
 */
UITools.TreeNode.prototype.markSelected = function() {
	// if one of nodes is selected
	if(this.rootNode.selectedNode) {
		// check if clicked node was not selected before
		if(this.rootNode.selectedNode==this) {
			return;
		}
		// unhighlight it
		this.rootNode.selectedNode.nodeLabel.className = 'nodeLabel';
	}
	// set selected classname
	this.nodeLabel.className = 'nodeLabelSelected';
	// create link to selected node
	this.rootNode.selectedNode = this;
	// execute event
    this.rootNode.fireEvent('select', {
        node: this
    });
};



/**
 * Changes icon
 * @param sUrl {string} String with image url
 */
UITools.TreeNode.prototype.changeIcon = function(sUrl) {
	if(sUrl && sUrl!='') {
		if(this.nodeIcon.firstChild) {
			this.nodeIcon.firstChild.src = sUrl;
		} else {
			this.nodeIcon.innerHTML = '<IMG src="' + sUrl + '">';
		}
	}
}

/**
 * Expands node
 */
UITools.TreeNode.prototype.expand = function(onExpand) {
	var self = this;
	if(this.childrenInitialized) {
		if(this.children.length) {
			// set class name to expanded
			var regs = this.rowLabel.className.match(/(.+)(Expanded|Collapsed|Loading)$/);
			this.rowLabel.className = regs[1] + 'Expanded';
			// chage icon to expanded
			this.changeIcon(this.config.iconExpanded);
			// show rendered elements
			this.rowChildren.style.display = '';
			// mark node as expanded
			this.expanded = true;
			// execute event
            this.rootNode.fireEvent('expand', {
                node: this
            });
			// Run callback
			if(onExpand) {
				onExpand(this);
			}
		}
	} else {
		// set class name to loading
		var regs = this.rowLabel.className.match(/(.+)(Expanded|Collapsed|Loading)$/);
		this.rowLabel.className = regs[1] + 'Loading';
		this.preload(function() {
			self.expand(onExpand);
		});
	}
}

/**
 * Collapse node
 */
UITools.TreeNode.prototype.collapse = function() {
	if(this.childrenInitialized) {
		if(this.children.length) {
			this.rowChildren.style.display = 'none';
			this.expanded = false;
			// set class name to collapsed
			var regs = this.rowLabel.className.match(/^(.+)(Expanded|Collapsed|Loading)$/);
			this.rowLabel.className = regs[1] + 'Collapsed';
			// chage icon to collapsed
			this.changeIcon(this.config.iconCollapsed);
			// execute event
            this.rootNode.fireEvent('collapse', {
                node: this
            });
		}
	}
};

/**
 * Preload node children 
 * @return Returns false if node doesn't have source to preload or sourceType is not url type
 */
UITools.TreeNode.prototype.preload = function(onPreload) {
	var self = this;
	
	if(!UITools.Transport) {
		alert('Preloading requires transport module to be loaded');
		return false;
	}
	
	// preload children
	new UITools.Transport({
	   	url: this.source,
   		method: 'GET',
   		onLoad: function(oResponse) {
  	   		oResponse = UITools.jsonDecode(oResponse);
			if(oResponse && oResponse.length) {
				for(var ii=0; ii<oResponse.length; ii++) {
					self.addChild(oResponse[ii]);
				}
				self.childrenInitialized = true;
				if(onPreload) {
					onPreload(self);
				}
			} else {
				alert('Incorrect JSON');
			}
   		},
		onError: function() {
			alert('Error');				
		}
	});	
}