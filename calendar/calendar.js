/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 * 
 * @fileoverview Javascript calendar module
 * 
 */

if (typeof UITools == 'undefined') {
	alert('uitools.js is required to run UITools.Calendar');
}

/**
 * Constructor of the calendar object
 * @constructor
 * @private
 * @param oArgs {object} Object configuration
 */
UITools.Calendar = function(oArgs) {
	// Call superclass constructor
	UITools.Calendar.superconstructor.call(this, oArgs);
};

// inherit basic functionality
UITools.inherit(UITools.Calendar, UITools.Widget);

/**
 * Short weekdays
 */
UITools.Calendar.prototype.weekDaysShort = [
	'Su','Mo','Tu','We','Th','Fr','Sa'
];

/**
 * Short weekdays
 */
UITools.Calendar.prototype.months = [
	'January','February','March','April','May','June','July','August','September','October','November','December'
];

/**
 * Initialize
 */
UITools.Calendar.prototype.init = function(oArgs) {
    UITools.Calendar.superclass.init.call(this, oArgs);
    
    // container is required
    this.container = this.element(oArgs.container);
    if(!this.isElement(this.container)) {
        alert('UITools.Calendar init failed: container missing or invalid');
    }
	// Theme
	this.theme = oArgs.theme ? oArgs.theme : 'default';
	// Weekday the week start with
	this.weekStart = oArgs.weekStart=='mon' ? 'mon' : 'sun';
 	// Parse today date
    if(typeof oArgs.todayDate == 'string') {
        this.todayDate = new Date(oArgs.todayDate);
    } else if(oArgs.todayDate instanceof Date) {
        this.todayDate = oArgs.todayDate;
    } else {
        this.todayDate = new Date();
    }
	// Parse current date
    if(typeof oArgs.currentDate == 'string') {
        this.currentDate = new Date(oArgs.currentDate);
    } else if(oArgs.currentDate instanceof Date) {
        this.currentDate = oArgs.currentDate;
    } else {
        this.currentDate = new Date();
    }

	// The month that should be displayed
	this.displayMonth = new Date();
	this.displayMonth.setFullYear(this.currentDate.getFullYear(), this.currentDate.getMonth(), 1);

	this.render(this.getViewDates(this.displayMonth));


};

/**
 * Calculates range of dates for the month view
 *
 * @private
 * @param dDisplayMonth {date} Display month date
 * @return oViewDates {object} 4 date objexts (monthStart, monthEnd, viewStart, viewEnd)
 */
UITools.Calendar.prototype.getViewDates = function(dDisplayMonth) {
	if(!(dDisplayMonth instanceof Date)) {
		return null;
	}
	// Month start
	var dMonthStart = new Date();
	dMonthStart.setFullYear(dDisplayMonth.getFullYear(),dDisplayMonth.getMonth(),1);
	dMonthStart.setHours(0,0,0);
	// Month end
	var dMonthEnd = new Date();
	dMonthEnd.setFullYear(dDisplayMonth.getFullYear(),dDisplayMonth.getMonth(),1);
	dMonthEnd.setHours(23,59,59);
	dMonthEnd.setMonth(dMonthEnd.getMonth() + 1); // 1 month forward
	dMonthEnd.setDate(dMonthEnd.getDate() - 1); // 1 day back
	// View start
	var dViewStart = new Date();
	dViewStart.setFullYear(dDisplayMonth.getFullYear(),dDisplayMonth.getMonth(),1);
	dViewStart.setHours(0,0,0);
	var iWeekDay = dViewStart.getDay(); // Get current weekday number
	dViewStart.setDate(dViewStart.getDate() - (this.weekStart=='mon' ? iWeekDay-1 : iWeekDay)); // Get the first day of the first week in the view (can be previous month day)
	// View end
	var dViewEnd = new Date();
	dViewEnd.setFullYear(dMonthEnd.getFullYear(), dMonthEnd.getMonth(), dMonthEnd.getDate());
	dViewEnd.setHours(23,59,59);
	var iWeekDay = dViewEnd.getDay(); // Get current weekday number
	dViewEnd.setDate(dViewEnd.getDate() + (this.weekStart=='mon' ? 6-iWeekDay+1 : 6-iWeekDay)); // Get the first day of the first week in the view (can be previous month day)
	return {
		monthStart: dMonthStart,
		monthEnd: dMonthEnd,
		viewStart: dViewStart,
		viewEnd: dViewEnd
	}
};

/**
 * Render calendar
 *
 * <pre>
 * Arguments format:
 * {
 *   monthStart: [date] First day of the month
 *   monthEnd: [date] Last day of the month
 *   viewStart: [date] The first day of the week that contains first day of the month
 *   viewEnd: [date] The last day of the week that contains last day of the month
 * }
 * </pre>
 *
 * @private
 * @param {object} oArg Arguments
 */
UITools.Calendar.prototype.render = function(oViewDates) {
	var self = this;
	var elTable, elHead, elRow, elCell, iWeekDay;

	var dWeek = new Date();
	var dWeekEnd = new Date();
	var dDay = new Date();

	// Create calendar table
	elTable = document.createElement('table')
	elTable.className = 'UTCalendar_' + this.theme;
	
	// Buttons head section
	elHead = document.createElement('thead');
	elRow = document.createElement('tr');
	
	// Previous button
	elCell = document.createElement('td');
	elCell.className = 'previous';
	elCell.innerHTML = '<div></div>';
	UITools.addEvent(elCell, 'mouseover', function(oEvent) {
		UITools.addClass(this, 'hover');
	}, false);
	UITools.addEvent(elCell, 'mouseout', function(oEvent) {
		UITools.removeClass(this, 'hover')
	}, false);
	UITools.addEvent(elCell, 'mousedown', function(oEvent) {
		UITools.addClass(this, 'down');
	}, false);
	UITools.addEvent(elCell, 'mouseup', function(oEvent) {
		UITools.removeClass(this, 'down');
	}, false);
	UITools.addEvent(elCell, 'click', function(oEvent) {
		self.prevMonth(oEvent);
	}, false);
	elRow.appendChild(elCell);

	// Title
	elCell = document.createElement('td');
	elCell.className = 'title';
	elCell.setAttribute('colspan', 5);
	elCell.innerHTML = '<div>' + this.months[oViewDates.monthStart.getMonth()] + ' ' + oViewDates.monthStart.getFullYear() + '</div>';
	elRow.appendChild(elCell);
	
	// Next button
	elCell = document.createElement('td');
	elCell.className = 'next';
	elCell.innerHTML = '<div></div>';
	UITools.addEvent(elCell, 'mouseover', function(oEvent) {
		UITools.addClass(this, 'hover');
	}, false);
	UITools.addEvent(elCell, 'mouseout', function(oEvent) {
		UITools.removeClass(this, 'hover')
	}, false);
	UITools.addEvent(elCell, 'mousedown', function(oEvent) {
		UITools.addClass(this, 'down');
	}, false);
	UITools.addEvent(elCell, 'mouseup', function(oEvent) {
		UITools.removeClass(this, 'down');
	}, false);
	UITools.addEvent(elCell, 'click', function(oEvent) {
		self.nextMonth(oEvent);
	}, false);
	elRow.appendChild(elCell);
	elHead.appendChild(elRow);
	elTable.appendChild(elHead);

	// Weekdays head section
	elHead = document.createElement('thead');
	elHead.className = 'weekdays';
	elRow = document.createElement('tr');
	elCell = document.createElement('td');
	if(this.weekStart=='mon') {
		for(iWeekDay=1; iWeekDay<7; iWeekDay++) {
			elCell = document.createElement('td');
			elCell.className = 'weekday';
			elCell.innerHTML = this.weekDaysShort[iWeekDay];
			elRow.appendChild(elCell);
		}
		elCell = document.createElement('td');
		elCell.className = 'weekday';
		elCell.innerHTML = this.weekDaysShort[0];
		elRow.appendChild(elCell);
	} else {
		for(iWeekDay=0; iWeekDay<7; iWeekDay++) {
			elCell = document.createElement('td');
			elCell.className = 'weekday';
			elCell.innerHTML = this.weekDaysShort[iWeekDay];
			elRow.appendChild(elCell);
		}
	}
	elHead.appendChild(elRow);
	elTable.appendChild(elHead);

	// Table body section
	var elBody = document.createElement('tbody');
	for(dWeek.setFullYear(oViewDates.viewStart.getFullYear(),oViewDates.viewStart.getMonth(),oViewDates.viewStart.getDate()); dWeek<=oViewDates.viewEnd; dWeek.setDate(dWeek.getDate() + 7)) {
		dWeekEnd.setFullYear(dWeek.getFullYear(), dWeek.getMonth(), dWeek.getDate());
		dWeekEnd.setDate(dWeekEnd.getDate() + 7)
		dWeekEnd.setHours(0,0,0);
		dDay.setHours(0,0,0);
		elRow = document.createElement('tr');

		for(dDay.setFullYear(dWeek.getFullYear(),dWeek.getMonth(),dWeek.getDate()); dDay<dWeekEnd; dDay.setDate(dDay.getDate() + 1)) {
			elCell = document.createElement('td');
			UITools.addClass(elCell, 'day');
			if(oViewDates.monthStart.getMonth()==dDay.getMonth()) {
				UITools.addClass(elCell, 'inside');
			} else {
				UITools.addClass(elCell, 'outside');
			}
			if(this.currentDate.getDate()==dDay.getDate() && this.currentDate.getMonth()==dDay.getMonth() && this.currentDate.getYear()==dDay.getYear()) {
				UITools.addClass(elCell, 'current');
			}
			if(this.todayDate.getDate()==dDay.getDate() && this.todayDate.getMonth()==dDay.getMonth() && this.todayDate.getYear()==dDay.getYear()) {
				UITools.addClass(elCell, 'today');
			}
			UITools.addEvent(elCell, 'mouseover', function(oEvent) {
				UITools.addClass(this, 'hover');
			}, false);
			UITools.addEvent(elCell, 'mouseout', function(oEvent) {
				UITools.removeClass(this, 'hover')
			}, false);
			UITools.addEvent(elCell, 'mousedown', function(oEvent) {
				UITools.addClass(this, 'down');
			}, false);
			UITools.addEvent(elCell, 'mouseup', function(oEvent) {
				UITools.removeClass(this, 'down');
			}, false);
            (function(year,month,day){
                UITools.addEvent(elCell, 'click', function(oEvent) {
                    self.dayClick(oEvent, year, month, day);
                }, false);
            })(dDay.getFullYear(), dDay.getMonth(), dDay.getDate());
			elCell.innerHTML = '<div>' + dDay.getDate() + '</div>';
			elRow.appendChild(elCell);
		}
		elBody.appendChild(elRow);
	}
	elTable.appendChild(elBody);
	this.container.innerHTML = '';
	this.container.appendChild(elTable);
    this.fireEvent('render', {
        calendar: this
    });
};

/**
 * Shift 1 month back
 *
 * @public
 */
UITools.Calendar.prototype.prevMonth = function(oEvent) {
	// Encrease month
	this.displayMonth.setMonth(this.displayMonth.getMonth()-1);
	var oViewDates = this.getViewDates(this.displayMonth);
	this.render(oViewDates);
    this.fireEvent('prevMonth', {
        calendar: this,
        event: oEvent,
        displayMonth: this.displayMonth.getMonth(),
        currentDate: this.currentDate
    });
};

/**
 * Shift 1 month front
 *
 * @public
 */
UITools.Calendar.prototype.nextMonth = function(oEvent) {
	// Encrease month
	this.displayMonth.setMonth(this.displayMonth.getMonth()+1);
	var oViewDates = this.getViewDates(this.displayMonth);
	this.render(oViewDates);
    this.fireEvent('nextMonth', {
        calendar: this,
        event: oEvent,
        displayMonth: this.displayMonth.getMonth(),
        currentDate: this.currentDate
    });
};

/**
 * Day click
 *
 * @private
 * @param iYear {integer} Year
 * @param iMonth {integer} Month
 * @param iDay {integer} Day
 */
UITools.Calendar.prototype.dayClick = function(oEvent, iYear, iMonth, iDay) {
	// redraw calendar
    this.setCurrentDate(iYear, iMonth, iDay);
    this.fireEvent('dayClick', {
        calendar: this,
        event: oEvent,
        displayMonth: this.displayMonth.getMonth(),
        currentDate: this.currentDate
    });
};

/**
 * Day click
 *
 * @private
 * @param iYear {integer} Year
 * @param iMonth {integer} Month
 * @param iDay {integer} Day
 */
UITools.Calendar.prototype.setCurrentDate = function() {
    if(arguments.length==1) {
        var cur = arguments[0];
        if(typeof cur == 'string') {
            cur = new Date(cur);
        }
        if(UITools.Date.valid(cur)) {
            this.currentDate.setFullYear(cur.getFullYear(), cur.getMonth(), cur.getDate());
        } else {
            return;
        }
    } else if(arguments.length==3) {
        this.currentDate.setFullYear(arguments[0], arguments[1], arguments[2]);
    } else {
        return;
    }
	// redraw calendar
	var oViewDates = this.getViewDates(this.displayMonth);
	this.render(oViewDates);
    this.fireEvent('currentDateChange', {
        calendar: this,
        displayMonth: this.displayMonth.getMonth(),
        currentDate: this.currentDate
    });
};