/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 * 
 * @fileoverview Javascript calendar module
 * 
 */

if (typeof UITools == 'undefined') {
	alert('uitools.js is required to run UITools.DatePicker');
}

/**
 * Constructor of the calendar object
 * @constructor
 * @private
 * @param oArgs {object} Object configuration
 */
UITools.DatePicker = function(oArgs) {
    // require Calendar
    if (typeof UITools.Calendar == 'undefined') {
        alert('UITools.Calendar is required to run UITools.DatePicker');
    }
    // required
    this.element = this.element(oArgs.element);
    if(!this.isElement(this.element)) {
        alert('UITools.DatePicker init failed: element missing or invalid');
    }
    // Date format, optional
    this.dateFormat = oArgs.dateFormat ? oArgs.dateFormat : '%m/%d/%Y';
	// Call superclass constructor
	UITools.DatePicker.superconstructor.call(this, oArgs);
};

UITools.DatePicker.prototype.id = 'UITools.DatePicker';

// inherit basic functionality
UITools.inherit(UITools.DatePicker, UITools.Widget);

/**
 * Initialize
 */
UITools.DatePicker.prototype.init = function() {
	var self = this;
    // Get field position
	var oElementOffset = UITools.getElementOffset(this.element);
    // Create container for calendar
	this.container = document.createElement('div');
	this.container.style.position = 'absolute';
	this.container.style.left = oElementOffset.left + 'px';
	this.container.style.top = (oElementOffset.top + oElementOffset.height) + 'px';
	this.container.style.display = 'none';
	UITools.insertAfter(this.container, this.element);

    // Assign events for the field
	UITools.addEvent(this.element, 'focus', function(oEvent) {
		self.container.style.display = 'block';
        self.calendar.setCurrentDate(self.element.value);
		UITools.stopEvent(oEvent);
	}, false);
	UITools.addEvent(this.element, 'blur', function(oEvent) {
        self.elementBlurTimeout = setTimeout(function(){
            self.container.style.display = 'none';
        }, 200);
		UITools.stopEvent(oEvent);
	}, false);
    
	this.calendar = new UITools.Calendar({
		theme: this.config.theme,
		container: this.container,
		weekStart: 'sun',
		eventListeners: {
            currentDateChange: function(oEvent) {
                self.element.value = UITools.Date.encode(oEvent.currentDate, self.dateFormat);
            },
            nextMonth: function(oEvent) {
                // Prevent hide of calendar
                clearTimeout(self.elementBlurTimeout);
                // Restore field focus
                self.element.focus();
            },
            prevMonth: function(oEvent) {
                // Prevent hide of calendar
                clearTimeout(self.elementBlurTimeout);
                // Restore field focus
                self.element.focus();
            }
		}
	});
};

