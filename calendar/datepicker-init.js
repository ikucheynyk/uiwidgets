(function() {
	UITools.addEvent(window, 'load', function() {
		var fields = UITools.getElementsByClassName('UIToolsDate');
		for(ii=0; ii<fields.length; ii++) {
			(function(field) {
				var oDatePicker = new UITools.DatePicker({
					theme: 'default',
					element: field,
					weekStart: 'sun'
				});
			})(fields[ii]);
		}

	});
})();
