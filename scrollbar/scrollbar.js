/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 * 
 * @fileoverview Javascript scrollbar module
 * 
 */

if (typeof UITools == 'undefined') {
	alert('uitools.js is required to run UITools.Tetris');
}

/**
 * Constructor of the scrollbar object
 * @constructor
 * @private
 * @param {object} Scrollbar configuration
 */
UITools.Scrollbar = function(oArgs) {
	// Call superclass constructor
	UITools.Scrollbar.superconstructor.call(this, oArgs);
};

// inherit basic functionality
UITools.inherit(UITools.Scrollbar, UITools.Widget);


/**
 * Initialize
 */
UITools.Scrollbar.prototype.init = function(oArgs) {
    UITools.Scrollbar.superclass.init.call(this, oArgs);

    // Container is required
    this.container = this.element(oArgs.container);
    if(!this.isElement(this.container)) {
        alert('UITools.Scrollbar init failed: container missing or invalid');
    }

    // Scrollbar container
    this.scrollbarContainer = this.element(oArgs.scrollbarContainer);
	// Theme
	this.theme = this.config.theme ? this.config.theme : 'default';

    // added by Andrew
    // Display method (fixed (default) | auto).
    // If 'fixed' then container for scrollbar always visible. If 'auto', then automatic calculate show or not container for scrollbar.
	this.display = this.config.display ? this.config.display : 'fixed';

	this.status = 'visible';

	this.render();
};


/**
 * Render scrollbar
 *
 * @private
 */
UITools.Scrollbar.prototype.render = function() {
	var self = this;
    this.scrollbarContainer = document.createElement('div');
    if (this.container.nextSibling) {
        this.container.parentNode.insertBefore(this.scrollbarContainer, this.container.nextSibling);
    } else {
        this.container.parentNode.appendChild(this.scrollbarContainer);
    }
	// Generate HTML layout
	this.scrollbarContainer.className = 'UITScrollbar_'+this.theme;
	this.scrollbarContainer.innerHTML = '<div>\
		<div class="ArrowUp" id="UITScrollbar' + this.objectId + 'ButtonTop"></div>\
		<div class="BarContainer" id="UITScrollbar' + this.objectId + 'BarContainer">\
			<div class="Slider" id="UITScrollbar' + this.objectId + 'Slider">\
				<div class="Top"></div>\
				<div class="Middle" id="UITScrollbar' + this.objectId + 'SliderMiddle">\
					<div></div>\
				</div>\
				<div class="Bottom"></div>\
			</div>\
		</div>\
		<div class="ArrowDown" id="UITScrollbar' + this.objectId + 'ButtonBottom"></div>\
	</div>';
	this.slider = document.getElementById('UITScrollbar' + this.objectId + 'Slider');
	this.sliderMiddle = document.getElementById('UITScrollbar' + this.objectId + 'SliderMiddle');
	this.barContainer = document.getElementById('UITScrollbar' + this.objectId + 'BarContainer');
	// Bar container events
    UITools.addEvent(this.container, 'DOMMouseScroll', function(oEvent) {
        if(!oEvent) oEvent=window.event;
        if(oEvent.detail>0) {
            self.scrollDown();
        } else {
            self.scrollUp();
        }
    });
    UITools.addEvent(this.barContainer, 'mousedown', function(event) {
        var oMouseCoords = self.mouseCoords(event);
        var oSliderOffset = UITools.getElementOffset(self.slider);
        // If mouse position is on the slider
        if(oMouseCoords.top < oSliderOffset.top + oSliderOffset.height
            && oMouseCoords.top > oSliderOffset.top
            && oMouseCoords.left < oSliderOffset.left + oSliderOffset.left
            && oMouseCoords.left > oSliderOffset.left) {
            // Start draggin it
            self.startDrag(event);
        } else {
            self.pageNavigation(event);
        }
    });
    UITools.addEvent(document, 'mouseup', function(event) {
        self.stopDrag(event);
    });
    UITools.addEvent(document, 'mousemove', function(event) {
        self.dragSlider(event);
    });
	this.buttonTop = document.getElementById('UITScrollbar' + this.objectId + 'ButtonTop');
	this.buttonBottom = document.getElementById('UITScrollbar' + this.objectId + 'ButtonBottom');
	// Top button events
	this.buttonTop.onmousedown = function(event) {
		self.keepScrolling = true;
		self.scrollUp();
	};
	this.buttonTop.onmouseup = function() {
		self.keepScrolling = false;
	};
	this.buttonTop.onmousemove = function() {
		self.keepScrolling = false;
	};
	// Bottom button events
	this.buttonBottom.onmousedown = function(event) {
		self.keepScrolling = true;
		self.scrollDown();
	};
	this.buttonBottom.onmouseup = function() {
		self.keepScrolling = false;
	};
	this.buttonBottom.onmousemove = function() {
		self.keepScrolling = false;
	};
    var oSliderOffset = UITools.getElementOffset(this.scrollbarContainer);
    this.scrollbarContainer.style.position = 'relative';
    this.scrollbarContainer.style.left = - oSliderOffset.width + 'px';

	// Set slider position
	this.updateSliderPosition();
};


/**
 * Updates slider box position and height on the scrollbar
 * it must reflect data length in the container
 *
 * @private
 */
UITools.Scrollbar.prototype.updateScrollbarPosition = function() {
	// Container must get thinner to get space for scrollbar
	var oContainerOffset = UITools.getElementOffset(this.container);
	// Set scrollbar height
	this.scrollbarContainer.style.height = oContainerOffset.height + 'px';
	var oButtonTopOffset = UITools.getElementOffset(this.buttonTop);
	var oButtonBottomOffset = UITools.getElementOffset(this.buttonBottom);
	var iSliderHeight = oContainerOffset.height - oButtonTopOffset.height - oButtonBottomOffset.height;
	this.barContainer.style.height = iSliderHeight + 'px';	
};

/**
 * Updates slider box position and height on the scrollbar
 * it must reflect data length in the container
 *
 * @private
 */
UITools.Scrollbar.prototype.updateSliderPosition = function() {
	// First slider height must be updated
	var oContainerOffset = UITools.getElementOffset(this.container);
	var iContainerScrollHeight = this.container.scrollHeight;
	// Check if scrollbar is needed, if not just hide it
	if(this.status=='visible' && iContainerScrollHeight <= Math.ceil(oContainerOffset.height)) {
        this.scrollbarContainer.style.display = 'none';
        this.status = 'hidden';
        // Run event
        this.fireEvent('hide');
	} else if(this.status=='hidden' &&  iContainerScrollHeight > Math.ceil(oContainerOffset.height)) {
        this.scrollbarContainer.style.display = 'block';
        this.status = 'visible';
        // Run event
        this.fireEvent('show');
	}
    if (this.status=='visible') {
		this.updateScrollbarPosition();
        var oBarContainerOffset = UITools.getElementOffset(this.barContainer);
    	// Calculate slider height
    	var iSliderMiddleHeight = Math.floor(oContainerOffset.height * oBarContainerOffset.height / iContainerScrollHeight);
    	this.sliderMiddle.style.height = iSliderMiddleHeight + 'px';
    	// Calculate slider position
    	var oSliderOffset = UITools.getElementOffset(this.slider);
    	var iSliderTop = Math.floor(this.container.scrollTop * oBarContainerOffset.height / iContainerScrollHeight);
    	if(iSliderTop+oSliderOffset.height > oBarContainerOffset.height) {
    		iSliderTop = oBarContainerOffset.height - oSliderOffset.height;
    	}
    	this.slider.style.top = iSliderTop + 'px';
	}
};


/**
 * Updates container scroll position according to position of the slider
 *
 * @private
 */
UITools.Scrollbar.prototype.updateContainerScrollPosition = function() {
	var oContainerOffset = UITools.getElementOffset(this.container);
	var oSliderOffset = UITools.getElementOffset(this.slider);
	var oBarContainerOffset = UITools.getElementOffset(this.barContainer);
	var sliderOffsetTop = oSliderOffset.top - oBarContainerOffset.top;
	var iContainerScrollHeight = this.container.scrollHeight;
	var iContainerScrollTop = Math.ceil((iContainerScrollHeight-oContainerOffset.height) * sliderOffsetTop / (oBarContainerOffset.height-oSliderOffset.height));
	this.container.scrollTop = iContainerScrollTop;
};

/**
 * Scrolls the container up
 *
 * @private
 */
UITools.Scrollbar.prototype.scrollUp = function() {
	var self = this;
    this.container.scrollTop -= 30;
    this.updateSliderPosition();
    if(this.keepScrolling) {
		setTimeout(function() {
			self.scrollUp();
		}, 100);
	}
};

/**
 * Scrolls the container down
 *
 * @private
 */
UITools.Scrollbar.prototype.scrollDown = function() {
	var self = this;
    this.container.scrollTop += 30;
    this.updateSliderPosition();
	if(this.keepScrolling) {
		setTimeout(function() {
			self.scrollDown();
		}, 100);
	}
};

/**
 * Called when mouse down on the slider
 *
 * @private
 */
UITools.Scrollbar.prototype.startDrag = function(event) {
	this.drag = true;
	var oSliderOffset = UITools.getElementOffset(this.slider);
	var oMousePosition  = this.mouseCoords(event);
	// Keep mouse offset
	this.mouseOffset = {
		left: oMousePosition.left - oSliderOffset.left, 
		top: oMousePosition.top - oSliderOffset.top
	};
};

/**
 * Called when mouse up on the slider
 *
 * @private
 */
UITools.Scrollbar.prototype.stopDrag = function() {
	this.drag = false;
	this.mouseOffset = null;
};

/**
 * Called when mouse moves over the slider
 *
 * @private
 */
UITools.Scrollbar.prototype.dragSlider = function(event) {
	if(this.drag) {
		var oMousePosition  = this.mouseCoords(event);
		var oBarContainerOffset = UITools.getElementOffset(this.barContainer);
		var oSliderOffset = UITools.getElementOffset(this.slider);
		var iSliderTop = oMousePosition.top - oBarContainerOffset.top - this.mouseOffset.top;
		if(iSliderTop < 0) {
			iSliderTop = 0;
		} else if(iSliderTop > (oBarContainerOffset.height - oSliderOffset.height)) {
			iSliderTop = oBarContainerOffset.height - oSliderOffset.height;
		}		
		this.slider.style.top = iSliderTop + 'px';
		this.updateContainerScrollPosition();
	}
};

/**
 * Return mouse coordinates
 *
 * @private
 */
UITools.Scrollbar.prototype.mouseCoords = function(event) {
	if(event.pageX || event.pageY){
		return {
			left: event.pageX,
			top: event.pageY
		};
	}
	return {
		left: event.clientX + document.body.scrollLeft - document.body.clientLeft,
		top: event.clientY + document.body.scrollTop  - document.body.clientTop
	};
};

/**
 * Jump 1 page up or down depending on mouse position (above or below the slider)
 *
 * @private
 */
UITools.Scrollbar.prototype.pageNavigation = function(event) {
	var oMouseCoords = this.mouseCoords(event);
	var oSliderOffset = UITools.getElementOffset(this.slider);
	if(oMouseCoords.top < oSliderOffset.top) {
		this.pageUp();
	} else if(oMouseCoords.top > oSliderOffset.top + oSliderOffset.height) {
		this.pageDown();
	}
};

/**
 * Moves the screen 1 page up
 *
 * @private
 */
UITools.Scrollbar.prototype.pageUp = function(event) {
	var oBarContainerOffset = UITools.getElementOffset(this.barContainer);
	var oSliderOffset = UITools.getElementOffset(this.slider);
	var iSliderTop = oSliderOffset.top - oBarContainerOffset.top - oSliderOffset.height;
	if(iSliderTop < 0) {
		iSliderTop = 0;
	} else if(iSliderTop > (oBarContainerOffset.height - oSliderOffset.height)) {
		iSliderTop = oBarContainerOffset.height - oSliderOffset.height;
	}
	this.slider.style.top = iSliderTop + 'px';
	this.updateContainerScrollPosition();
};

/**
 * Moves the screen 1 page down
 *
 * @private
 */
UITools.Scrollbar.prototype.pageDown = function(event) {
	var oBarContainerOffset = UITools.getElementOffset(this.barContainer);
	var oSliderOffset = UITools.getElementOffset(this.slider);
	var iSliderTop = oSliderOffset.top - oBarContainerOffset.top + oSliderOffset.height;
	if(iSliderTop > (oBarContainerOffset.height - oSliderOffset.height)) {
		iSliderTop = oBarContainerOffset.height - oSliderOffset.height;
	}
	this.slider.style.top = iSliderTop + 'px';
	this.updateContainerScrollPosition();
};