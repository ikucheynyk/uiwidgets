/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 * 
 * @fileoverview Javascript calendar module
 * 
 */

if (typeof UITools == 'undefined') {
	alert('uitools.js is required to run UITools.Tetris');
}

/**
 * Constructor of the calendar object
 * @constructor
 * @private
 * @param oArgs {object} Object configuration
 */
UITools.Tetris = function(oArgs) {
	// Call superclass constructor
	UITools.Tetris.superconstructor.call(this, oArgs);
};

// inherit basic functionality
UITools.inherit(UITools.Tetris, UITools.Widget);

UITools.Tetris.prototype.figures = [
    [
        [1,1,1,1]
    ],
    [
        [1,1,1],
        [0,0,1]
    ],
    [
        [1,1],
        [1,1]
    ],
    [
        [0,1,0],
        [1,1,1]
    ],
    [
        [0,1],
        [1,1],
        [1,0]
    ],
    [
        [1,0],
        [1,1],
        [0,1]
    ]
];
UITools.Tetris.prototype.matrix = [];
UITools.Tetris.prototype.elements = [];

/**
 * Initialize
 */
UITools.Tetris.prototype.init = function(oArgs) {
    UITools.Tetris.superclass.init.call(this, oArgs);
    
    // container is required
    this.container = this.element(oArgs.container);
    if(!this.isElement(this.container)) {
        alert('UITools.Tetris init failed: container missing or invalid');
    }
	this.width = oArgs.width ? oArgs.width : 10;
	this.height = oArgs.height ? oArgs.height : 10;
    this.fieldOffset = UITools.getElementOffset(this.container);
    this.squareWidth = this.fieldOffset.width / this.width;
    this.squareHeight = this.fieldOffset.height / this.height;

    this.container.className = 'UTTetris_' + this.theme;

	this.start();
};

UITools.Tetris.prototype.createFigure = function() {
    // Randomly get figure number
    var iFigure = Math.floor(Math.random() * ((this.figures.length-1) + 1));
    var aFigure = this.figures[iFigure];
    console.log('iFigure: '+iFigure);
    // Create figure elements
	// Previous button
	var eContainer = document.createElement('div');
	eContainer.className = 'figure';
    this.container.appendChild(eContainer);
    var self = this;
    // Render figure
    var oFigure = {
        tetris: this,
        index: iFigure,
        matrix: aFigure,
        width: 0,
        height: 0,
        left: 0,
        top: 0,
        container: eContainer,
        elements: [],
        init: function() {
            this.width = this.matrix[0].length;
            this.height = this.matrix.length;
            // place to the start position
            this.top = 0;
            this.left = Math.round(this.tetris.width / 2 - (this.width+1) / 2);
            if(this.positionValid(this.top, this.left)) {
                this.container.style.left = this.tetris.fieldOffset.left + this.left * this.tetris.squareWidth;
                this.container.style.top = this.tetris.fieldOffset.top;
                // render
                this.render();
            } else {
                this.tetris.gameStatus = 0;
            }
        },
        render: function() {
            // Create square elements
            var y, x, eSqare;
            for(y=0; y<this.height; y++) {
                for(x=0; x<this.width; x++) {
                    if(this.matrix[y][x]) {
                        eSqare = document.createElement('div');
                        eSqare.style.width = this.tetris.squareWidth + 'px';
                        eSqare.style.height = this.tetris.squareHeight + 'px';
                        eSqare.style.left = this.tetris.squareWidth * x;
                        eSqare.style.top = this.tetris.squareHeight * y;
                        eContainer.appendChild(eSqare);
                    }
                }
            }
        },
        positionValid: function(y, x) {
            if(x < 0 || x+this.width > this.tetris.width) {
                return false;
            }
            var xx, yy;
            for(yy=0; yy<this.height; yy++) {
                for(xx=0; xx<this.width; xx++) {
                    if(this.matrix[yy][xx]) {
                        if((y+yy)>this.tetris.height-1 || (y+yy)>=this.tetris.height || (x+xx)>=this.tetris.width || this.tetris.matrix[y+yy][x+xx]) {
                            console.log('check validity');
//                            console.log((y+yy) +'>='+this.tetris.height+' || '+(x+xx)+'>='+this.tetris.width+' || '+this.tetris.matrix[y+yy][x+xx]);
                            console.log('y='+y+', yy='+yy+', x='+x+', xx='+xx);
                            console.log('this.tetris.matrix['+(y+yy)+']['+(x+xx)+']');
                            return false;
                        }
                    }
                }
            }
            return true;
        },
        moveDown: function() {
            if(this.positionValid(this.top+1, this.left)) {
                this.top = this.top + 1;
                this.container.style.top = this.tetris.fieldOffset.top + this.top * this.tetris.squareHeight;
                return true;
            }
            return false;
        },
        moveLeft: function() {
            if(this.positionValid(this.top, this.left-1)) {
                this.left--;
                this.container.style.left = this.tetris.fieldOffset.left + this.left * this.tetris.squareWidth;
                return true;
            }
            return false;
        },
        moveRight: function() {
            if(this.positionValid(this.top, this.left+1)) {
                this.left++;
                this.container.style.left = this.tetris.fieldOffset.left + this.left * this.tetris.squareWidth;
                return true;
            }
            return false;
        },
        rotateRight: function() {
            var newMatrix = [];
            var xx,yy;
            for(yy=0; yy<this.matrix[0].length; yy++) {
                newMatrix[yy] = [];
            }
            for(yy=this.matrix.length-1,x=0; yy>=0; yy--,x++) {
                for(xx=0,y=0; xx<this.matrix[0].length; xx++,y++) {
                    newMatrix[y][x] = this.matrix[yy][xx];
                }
            }
            this.matrix = newMatrix;
            this.width = this.matrix[0].length;
            this.height = this.matrix.length;
            if(this.left + this.width > this.tetris.width) {
                this.left = this.tetris.width - this.width;
                this.container.style.left = this.tetris.fieldOffset.left + this.left * this.tetris.squareWidth;
            }
            this.container.innerHTML = '';
            this.render();
        },
        destroy: function() {
            this.matrix = null;
            this.container.parentNode.removeChild(this.container);
        }
    };
    oFigure.init();
    return oFigure;
};

UITools.Tetris.prototype.start = function() {
    this.createMatrix();
    this.gameStatus = 1;
    this.run();
    var self = this;
    UITools.addEvent(document, 'keydown', function(oEvent) {
        if(!self.currentFigure) {
            return;
        }
        switch (oEvent.keyCode) {
            // Left
            case 37: {
                self.currentFigure.moveLeft();
                break;
            }
            // Right
            case 39: {
                self.currentFigure.moveRight();
                break;
            }
            // DOwn
            case 40: {
                self.currentFigure.moveDown();
                break;
            }
            // Up
            case 38: {
                self.currentFigure.rotateRight();
                break;
            }
        }
    })
};

UITools.Tetris.prototype.run = function() {
    if(!this.currentFigure) {
        this.currentFigure = this.createFigure();
    } else if(!this.currentFigure.moveDown()) {
        this.freeze(this.currentFigure);
        delete this.currentFigure;
    }

    var self = this;
    setTimeout(function() {
        if(self.gameStatus) {
            self.run();
        } else {
            console.log('game over');
        }
    }, 500);
};

UITools.Tetris.prototype.freeze = function(oFigure) {
    var xx, yy, ii, jj, removeRow, newMatrix, newElements, el;
    var y = oFigure.top;
    var x = oFigure.left;
    for(yy=0; yy<oFigure.height; yy++) {
        for(xx=0; xx<oFigure.width; xx++) {
            if(oFigure.matrix[yy][xx]) {
                this.matrix[y+yy][x+xx] = 1;
                var eSquare = document.createElement('div');
                eSquare.style.top = (y+yy) * this.squareHeight + this.fieldOffset.top;
                eSquare.style.left = (x+xx) * this.squareWidth + this.fieldOffset.left;
                eSquare.style.width = this.squareWidth + 'px';
                eSquare.style.height = this.squareHeight + 'px';
                this.elements[y+yy].push(eSquare);
                this.container.appendChild(eSquare);
            }
        }
    }
    oFigure.destroy();
    // define empty row
    var emptyRow = [];
    for(xx=0; xx<this.width; xx++) {
        emptyRow[xx] = 0;
    }
    // search for fully filled rows
    for(yy=this.height-1; yy>=0; yy--) {
        removeRow = true;
        for(xx=0; xx<this.width; xx++) {
            if(!this.matrix[yy][xx]) {
                removeRow = false;
            }
        }
        // row found
        if(removeRow) {
            // Delete row
            console.log('delete row: '+yy);
            newMatrix = [emptyRow];
            console.log(newMatrix);
            newElements = [[]];
            for(ii=0; ii<this.height; ii++) {
                if(ii!=yy) {
                    newMatrix.push(this.matrix[ii]);
//                    newElements.push(this.elements[ii]);
                } else {
//                    for(jj=0; jj<this.elements[ii].length; jj++) {
//                        this.elements[ii][jj].parentNode.removeChild(this.elements[ii][jj]);
//                    }
                }
            }
            // shift rows down
            this.matrix = newMatrix;
//            this.elements = newElements;
//            for(ii=0; ii<=yy; ii++) {
//                for(jj=0; jj<this.elements[ii].length; jj++) {
//                    this.elements[ii][jj].style.top = UITools.getElementOffset(this.elements[ii][jj]).top + this.squareHeight;
//                }
//            }
            yy++;
        }
    }
    // Clear matrix
    var len = this.container.childNodes.length;
    for(ii=len-1; ii>=0; ii--) {
        this.container.removeChild(this.container.childNodes[ii]);
    }
    // Render it again
    for(ii=0; ii<this.height; ii++) {
        for(jj=0; jj<this.width; jj++) {
            if(this.matrix[ii][jj]) {
                el = document.createElement('div');
                el.style.top = this.fieldOffset.top + ii * this.squareHeight;
                el.style.left = this.fieldOffset.left + jj * this.squareWidth;
                el.style.height = this.squareHeight + 'px';
                el.style.width = this.squareWidth + 'px';
                this.container.appendChild(el);
            }
        }
    }

};

UITools.Tetris.prototype.createMatrix = function() {
    this.matrix = [];
    this.elements = [];
    this.container.innerHTML = '';
    var x,y,row;
    for(y=0; y<this.height; y++) {
        this.matrix[y] = [];
        this.elements[y] = [];
        for(x=0; x<this.width; x++) {
            this.matrix[y][x] = 0;
        }
    }
};
