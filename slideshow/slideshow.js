/**
 * @author Igor Kucheinyk (igorok@igorok.com)
 * 
 * @fileoverview Slideshow
 * 
 * Example of usage
 * <pre>
 * 			var slideShow = new UITools.Slideshow({
 * 				target: document.getElementById("slideshowContainer"),   // Required. Target element  
 * 				images: [
 * 					'img1.jpg',
 * 					'img2.jpg',
 * 					'img3.jpg',
 * 					'img4.jpg',
 * 					'img5.jpg',
 * 					'img6.jpg',
 * 					'img7.jpg'
 * 				]
 * 			});		
 * </pre> 
 * 
 * 
 */

if (typeof UITools == 'undefined') {
	alert('uitools.js is required to run UITools.Slideshow');
}

/**
 * Constructor of the object
 * @constructor
 * @param oArgs {object} descr
 */
UITools.Slideshow = function(oArgs) {
    UITools.Slideshow.superconstructor.call(this, oArgs);
};

// inherit basic functionality
UITools.inherit(UITools.Slideshow, UITools.Widget);


UITools.Slideshow.prototype.init = function(oArgs) {
    UITools.Slideshow.superclass.init.call(this, oArgs);
    
	// Check image array
	if(!oArgs.images || !UITools.isArray(oArgs.images)) {
		alert('Error wile initializing slideshow, `images` must be array of image strings');
		return;
	}
    // container is required
    this.container = this.element(oArgs.container);
    if(!this.isElement(this.container)) {
        alert('UITools.Menu init failed: container missing or invalid');
    }
	this.delay = oArgs.delay ? oArgs.delay : 10000;
	// Define appear container
	var containerOffset = UITools.getElementOffset(this.container);
	if(oArgs.appearContainer) {
		this.appearContainer = oArgs.appearContainer;
	} else {
		var appearElement = document.createElement('DIV');
		if(this.container.nextSibling) {
			this.container.parentNode.insertBefore(appearElement, this.container.nextSibling)
		} else {
			this.container.parentNode.appendChild(appearElement);
		}
		this.appearContainer = appearElement;
	}
	this.container.style.backgroundPosition = 'center center';
	this.container.style.backgroundRepeat = 'no-repeat';
	this.appearContainer.style.position = 'absolute';
	this.appearContainer.style.top = containerOffset.top + 'px';
	this.appearContainer.style.left = containerOffset.left + 'px';
	this.appearContainer.style.backgroundPosition = 'center center';
	this.appearContainer.style.backgroundRepeat = 'no-repeat';
	this.appearContainer.style.width = containerOffset.width + 'px';
	this.appearContainer.style.height = containerOffset.height + 'px';
	this.imageUrls = oArgs.images;
	this.imageElems = [];
	this.imageComplete = [];
	// Define elements
	this.defineImageElements();
	// Preload images
	this.preloadOneByOne(0);
	this.start(0);
};

/**
 * Preload all images
 */
UITools.Slideshow.prototype.defineImageElements = function() {
	var self = this;
	var iImage, iImageLength;
	iImageLength = this.imageUrls.length;
	for (iImage=0; iImage<iImageLength; iImage++) {
		this.imageElems[iImage] = new Image(20,20);
	}
};

/**
 * Preloads the image
 * @param iImage {number} Id of image in the slideshow.imageElems array
 * @param onLoad {function} Callback function, will be called after image loaded
 */
UITools.Slideshow.prototype.preloadOneByOne = function(iImage) {
	if(!this.imageElems[iImage]) {
		return;
	}
	var self = this;
	var onLoad = function() {
		self.imageComplete[iImage] = true;
		self.preloadOneByOne(iImage+1);
	}
	var onError = function() {
		self.preloadOneByOne(iImage+1);
	}
	UITools.addEvent(this.imageElems[iImage], 'load', onLoad, false);
	UITools.addEvent(this.imageElems[iImage], 'error', onError, false);
	this.imageElems[iImage].src = this.imageUrls[iImage];
}

/**
 * Starts slideshow
 * @param iImage {number} Id of image in the slideshow.imageElems array
 */
UITools.Slideshow.prototype.start = function(iImage) {
	var self = this;
	if(this.imageElems[iImage]) {
		this.displayImage(iImage, function() {
			// Display next image in few seconds
			setTimeout(function() {
				if(self.imageElems.length > iImage+1) {
					self.start(iImage+1);
				} else {
					self.start(0);
				}
			}, self.delay);
		});
	}
}

/**
 * Preloads the image
 * @param iImage {number} Id of image in the slideshow.imageElems array
 */
UITools.Slideshow.prototype.displayImage = function(iImage, fImageDisplayed) {
	var containerOffset = UITools.getElementOffset(this.container);
	this.appearContainer.style.top = containerOffset.top + 'px';
	this.appearContainer.style.left = containerOffset.left + 'px';

	var self = this;
	// If image already cached
	if(this.imageComplete[iImage]) {
		// Just display it
		//this.container.style.backgroundImage = 'url('+this.imageUrls[iImage]+')';
		// Display it softly
		var elContainer = this.container;
		var elChildContainer = this.appearContainer;

		// Hide appear container
		this.setElementOpacity(elChildContainer, 0);

		elChildContainer.style.backgroundImage = 'url('+this.imageUrls[iImage]+')';
		this.fadeIn(0, function() {
			elContainer.style.backgroundImage = 'url('+self.imageUrls[iImage]+')';
			self.setElementOpacity(elContainer, 1);
			elChildContainer.style.backgroundImage = 'none';
			self.setElementOpacity(elChildContainer, 0);

			if(fImageDisplayed) {
				fImageDisplayed(iImage);
			}
		});
	} else {
		var onLoad = function() {
			self.imageComplete[iImage] = true;
			self.displayImage(iImage, fImageDisplayed);
		}
		UITools.addEvent(this.imageElems[iImage], 'load', onLoad, false);
		UITools.addEvent(this.imageElems[iImage], 'error', onLoad, false);
	}
}

UITools.Slideshow.prototype.setElementOpacity = function(oElement, opacity) {
	if(SS.is_ie) {
		oElement.style.filter="alpha(opacity='" + (100 * opacity) + "')";
	} else {
		oElement.style.opacity = opacity;
	}
}

UITools.Slideshow.prototype.fadeIn = function(ii, callback) {
	var self = this;
	var container = this.container;
	var elChildContainer = this.appearContainer;
	this.setElementOpacity(container, 1 - (ii/10));
	this.setElementOpacity(elChildContainer, ii/10);
	if(ii>=10) {
		if(callback) {
			callback();
		}
		return;
	}
	// Next loop
	setTimeout(function() {
		self.fadeIn(ii+0.5, callback);
	}, 50)
}
